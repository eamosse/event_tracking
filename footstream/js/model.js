var Event = function(game, event){
	var thiz = this; 
	thiz.game = game;
	thiz.action =  event.action; 
	thiz.team = event.team; 
	thiz.time = event.time; 
	thiz.type = event.type; 
	thiz.player = event.player; 
	thiz.related = event.related; 

	thiz.resume = function(){
		switch(thiz.action) {
			case "D1P": 
				return "KICK OFF! Here we go for " + thiz.game.home + " " + thiz.game..away; 
			case "F1P" : 
				return "Half time! " + thiz.game.home + " " + thiz.game.score + " " + scorevent.away; 
			case "F1P" : 
				return "The second period between " + thiz.game.home + " and " + scorevent.away " has began!"; 
			case "F2P": 
				return "Full time! " + thiz.game.home + " " + thiz.game.score + " " + scorevent.away; 
			case "TIR":
				return "Shoot by " + thiz.player + " for " + thiz.team;
			case "CJA":
				return thiz.player + " is booked for a fault!";
			case "CRO":
				return thiz.player + " is sent off the game!";
			case "BUT":
				return "GOAL!!!! " + thiz.game.home + " " + thiz.game.score + " " + thiz.game.away;
			case "CGT":
				return thiz.player + " is coming on for " + thiz.related;
			default: 
				return thiz.action;
		}
	}
}

/*eventList.game = {
  "away": "Wales",
  "start": "13",
  "type": "init",
  "events": [
    {
      "action": "D1P",
      "time": "15:00",
      "type": "update",
      "$$hashKey": "object:3"
    },
    {
      "action": "TIR",
      "time": "15:09",
      "team": "England",
      "type": "update",
      "player": "Sterling",
      "$$hashKey": "object:5"
    },
    {
      "action": "TIR",
      "time": "15:17",
      "team": "England",
      "type": "update",
      "player": "Sterling",
      "$$hashKey": "object:7"
    },
    {
      "action": "TIR",
      "time": "15:24",
      "team": "England",
      "type": "update",
      "player": "Kane",
      "$$hashKey": "object:9"
    },
    {
      "action": "TIR",
      "time": "15:28",
      "team": "England",
      "type": "update",
      "player": "Rooney",
      "$$hashKey": "object:11"
    },
    {
      "action": "TIR",
      "time": "15:29",
      "team": "England",
      "type": "update",
      "player": "Sterling",
      "$$hashKey": "object:13"
    },
    {
      "action": "TIR",
      "time": "15:43",
      "team": "Wales",
      "type": "update",
      "player": "Bale",
      "$$hashKey": "object:15"
    },
    {
      "score": "1_0",
      "action": "BUT",
      "time": "15:44",
      "team": "Wales",
      "type": "update",
      "player": "Bale",
      "$$hashKey": "object:17"
    },
    {
      "action": "TIR",
      "time": "15:43",
      "team": "England",
      "type": "update",
      "player": "Hart",
      "$$hashKey": "object:19"
    }
  ],
  "home": "England"
}
evts = [];
for (e in this.game.events){
    e = new Event(this.game, e);
    evts.push(e);
}
this.game.events = evts;
*/