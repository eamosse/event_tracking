var Event = function(game, event){
    var thiz = this; 
    thiz.game = game;
    thiz.action =  event.action; 
    thiz.team = event.team; 
    thiz.time = event.time; 
    thiz.type = event.type; 
    thiz.player = event.player; 
    thiz.related = event.related; 

    thiz.resume = function(){
        switch(thiz.action) {
            case "D1P": 
                return "KICK OFF! Here we go for " + thiz.game.home + " " + thiz.game.away; 
            case "F1P" : 
                return "Half time! " + thiz.game.home + " " + thiz.game.score + " " + scorevent.away; 
            case "F1P" : 
                return "The second period between " + thiz.game.home + " and " + scorevent.away + " has began!"; 
            case "F2P": 
                return "Full time! " + thiz.game.home + " " + thiz.game.score + " " + scorevent.away; 
            case "TIR":
                return "Shoot by " + thiz.player + " for " + thiz.team;
            case "CJA":
                return thiz.player + " is booked for a fault!";
            case "CRO":
                return thiz.player + " is sent off the game!";
            case "BUT":
                return "GOAL!!!! " + thiz.game.home + " " + thiz.game.score + " " + thiz.game.away;
            case "CGT":
                return thiz.player + " is coming on for " + thiz.related;
            default: 
                return thiz.action;
        }
    }
    return thiz;
}

var webSocket;
var messages = document.getElementById("messages");
var soccerEventApp = angular.module('soccerEventApp', []);
  soccerEventApp.controller('soccerEventController', function($scope) {

var eventList = this;
  
    eventList.game = {
                    "home":{name : "England", scorers : []}, 
                    "away": {name:"Wales", scorers : [],},
                    "events" :[]
                };

    eventList.score = function() {
        return eventList.game.home.scorers.length + " - " + eventList.game.away.scorers.length;
    }; 

    eventList.add = function(event) {

        console.log(event);
        
        $scope.$apply(function(){
             if (event['type'] === 'init'){
                event.home = {'name' : event.home, 'scorers' : []};
                event.away = {'name' : event.away, 'scorers' : []};
                evts = []; 
                for (evt in event.events) {
                    scorer = {'player' : evt.player, 'time': evt.time};
                    if (evt.action ==="BUT"){
                        if (evt.team === event.name) {
                        event.home.scorers.push(scorer);
                    }else{
                        event.away.scorers.push(scorer);
                    }
                    }
                }
                event.events = evts;
                $scope.eventList.game = event;
        }

        if (event['type'] === 'update'){
            if (event.score) {
                eventList.game.score = event.score;
                scorer = {'player' : event.player, 'time': event.time};
                if (event.team === eventList.game.name) {
                    eventList.game.home.scorers.push(scorer);
                }else{
                    eventList.game.away.scorers.push(scorer);
                }
            }
            $scope.eventList.game.events.push(event);
            }
        });
        
    };

    eventList.resume = function(thiz){
        switch(thiz.action) {
            case "D1P": 
                return "KICK OFF! Here we go for " + eventList.game.home.name + " " + eventList.game.away.name; 
            case "F1P" : 
                return "Half time! " + eventList.game.home.name + " " + eventList.score() + " " + eventList.game.away.name; 
            case "F1P" : 
                return "The second period between " + thiz.game.home.name + " and " + eventList.game.away.name + " has began!"; 
            case "F2P": 
                return "Full time! " + eventList.game.home.name + " " + eventList.score() + " " + eventList.game.away.name; 
            case "TIR":
                return "Shoot by " + thiz.player + " for " + thiz.team;
            case "CJA":
                return thiz.player + " is booked for a fault!";
            case "CRO":
                return thiz.player + " is sent off the game!";
            case "BUT":
                return "GOAL!!!! " + eventList.game.home.name + " " + eventList.score() + " " + eventList.game.away.name;
            case "CGT":
                return thiz.player + " is coming on for " + thiz.related;
            default: 
                return thiz.action;
        }
    }

    openSocket(eventList); 

  });

function parseEvent(event) {

}

function openSocket(eventList){
    // Ensures only one connection is open at a time
    if(webSocket !== undefined && webSocket.readyState !== WebSocket.CLOSED){
        writeResponse("WebSocket is already opened.");
        return;
    }
    // Create a new instance of the websocket
    webSocket = new WebSocket("ws://localhost:8080/socket/echo");
     
    /**
     * Binds functions to the listeners for the websocket.
     */
    webSocket.onopen = function(event){
        // For reasons I can't determine, onopen gets called twice
        // and the first time event.data is undefined.
        // Leave a comment if you know the answer.
        if(event.data === undefined) 
            return;
        
        writeResponse(event.data);
    };

    webSocket.onmessage = function(event){
        //writeResponse(event.data);
        eventList.add(JSON.parse(event.data));
    };

    webSocket.onclose = function(event){
        writeResponse("Connection closed");
    };
}

function send(message){
    webSocket.send(message);
            }

function closeSocket(){
    webSocket.close();
}

function writeResponse(text){
    messages.innerHTML += "<br/>" + text;

}