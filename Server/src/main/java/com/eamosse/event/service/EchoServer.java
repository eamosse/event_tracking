/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.event.service;

import com.eamosse.these.event.StreamAnalyzer;
import com.eamosse.these.event.models.ActionResult;
import com.eamosse.these.event.models.Game;
import com.eamosse.these.event.models.Event;
import com.eamosse.these.event.mongodb.JsonHelper;
import com.eamosse.these.event.server.ActionCallback;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @ServerEndpoint gives the relative name for the end point This will be
 * accessed via ws://localhost:8080/EchoChamber/echo Where "localhost" is the
 * address of the host, "EchoChamber" is the name of the package and "echo" is
 * the address to access this class from the server
 */
@ServerEndpoint("/echo")
public class EchoServer {

    StreamAnalyzer stream;
    ArrayList<Session> sessions = new ArrayList<>();

    /**
     * @param session
     * @OnOpen allows us to intercept the creation of a new session. The session
     * class allows us to send data to the user. In the method onOpen, we'll let
     * the user know that the handshake was successful.
     */
    @OnOpen
    public void onOpen(Session session) {
        stream = StreamAnalyzer.getInstance();
        sessions.add(session);
        /*if (stream.game == null) {
            stream.game = new Game(args[0], args[1], args[2]);
        }*/
        stream.callbacks.add(new ActionCallback() {
            @Override
            public void onResult(Event action) {
                ActionResult result = action.toResult();
                //stream.game.events.add(result);
                for (Session _session : sessions) {
                    sendMessage(_session, result, "update");
                }
            }

            @Override
            public void onIteration(Object object) {
                for (Session _session : sessions) {
                    sendMessage(_session, object, "stat");
                }
            }
        });
        if (stream.game != null) {
            sendMessage(session, stream.game, "init");
        }
    }

    void sendMessage(Session session, Object content, String type) {
        if (session.isOpen()) {
            try {
                JSONObject ob = JsonHelper.createJsonObject(content);
                ob.put("type", type);
                session.getBasicRemote().sendText(ob.toString());
            } catch (IOException | JSONException ex) {
                Logger.getLogger(EchoServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * When a user sends a message to the server, this method will intercept the
     * message and allow us to react to it. For now the message is read as a
     * String.
     *
     * @param message
     * @param session
     */
    @OnMessage
    public void onMessage(String message, Session session) {
        System.out.println("Message from " + session.getId() + ": " + message);
        Game game = JsonHelper.getObjectFromJson(message, Game.class);
        switch (game.status) {
            case 0:
                stream.game = game;
                break;
            case -1:
                stream.stop();
                break;
            case 1:
                game.start -= 2;
                stream.game = game;
                stream.game.events = new ArrayList<>();
                stream.restart();
                break;
        }
        sendMessage(session, game, "init");
    }

    /**
     * The user closes the connection.
     *
     * Note: you can't send messages to the client from this method
     *
     * @param session
     */
    @OnClose
    public void onClose(Session session) {
        System.out.println("Session " + session.getId() + " has ended");
        sessions.remove(session);
    }
}
