/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.event.service.services;

import com.eamosse.these.event.models.Game;
import com.eamosse.these.event.mongodb.DBManager;
import com.google.gson.reflect.TypeToken;
import com.mongodb.BasicDBObject;
import java.lang.reflect.Type;
import java.util.ArrayList;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author aedouard
 */
@Path("match")
public class MatchResources {

    Type listType = new TypeToken<ArrayList<Game>>() {
    }.getType();

    @GET
    @Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
    public ArrayList<Game> games() {
        ArrayList<Game> games = DBManager.find("match", new BasicDBObject(), listType);
        return games;
    }

}
