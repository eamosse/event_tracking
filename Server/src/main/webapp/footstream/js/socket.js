var Event = function (game, event) {
    var thiz = this;
    thiz.game = game;
    thiz.action = event.action;
    thiz.team = event.team;
    thiz.time = event.time;
    thiz.type = event.type;
    thiz.player = event.player;
    thiz.related = event.related;

    thiz.resume = function () {
        switch (thiz.action) {
            case "D1P":
                return "KICK OFF! Here we go for " + thiz.game.home + " " + thiz.game.away;
            case "F1P" :
                return "Half time! " + thiz.game.home + " " + thiz.game.score + " " + scorevent.away;
            case "F1P" :
                return "The second period between " + thiz.game.home + " and " + scorevent.away + " has began!";
            case "F2P":
                return "Full time! " + thiz.game.home + " " + thiz.game.score + " " + scorevent.away;
            case "TIR":
                return "Shoot by " + thiz.player + " for " + thiz.team;
            case "CJA":
                return thiz.player + " is booked for a fault!";
            case "CRO":
                return thiz.player + " is sent off the game!";
            case "BUT":
                return "GOAL!!!! " + thiz.game.home + " " + thiz.game.score + " " + thiz.game.away;
            case "CGT":
                return thiz.player + " is coming on for " + thiz.related;
            default:
                return thiz.action;
        }
    }
    return thiz;
};




var webSocket;
var messages = document.getElementById("messages");
var soccerEventApp = angular.module('soccerEventApp', []);

soccerEventApp.controller('soccerEventController', function ($scope, $http) {

    var eventList = this;
    $http.get('webresources/match/').
            then(function (response) {
                //console.log(response.data);
                $scope.matches = response.data;
                $scope.game = $scope.matches[0];

                eventList.score = function () {
                    return $scope.game.home.scorers.length + " - " + $scope.game.away.scorers.length;
                };

                eventList.add = function (event) {
                    event = angular.fromJson(event);
                    $scope.$apply(function () {
                        if (event['type'] === 'init') {
                            $scope.game.parameters = event['parameters'];
                            evts = [];
                            for (var evt in event.events) {
                                evt = event.events[evt];
                                scorer = {'player': evt.player, 'time': evt.time};
                                if (evt.action === "BUT") {
                                    if (evt.team === $scope.game.home.name) {
                                        event.home.scorers.push(scorer);
                                    } else {
                                        event.away.scorers.push(scorer);
                                    }
                                }
                                evts.push(evt);
                            }
                            event.events = evts;
                            $scope.game = event;
                        }

                        if (event['type'] === 'update') {
                            if (event.score) {
                                $scope.game.score = event.score;
                            }
                            if (event.action === "BUT") {
                                scorer = {'player': event.player, 'time': event.time};
                                if (event.team === eventList.game.home.name) {
                                    $scope.game.home.scorers.push(scorer);
                                } else {
                                    $scope.game.away.scorers.push(scorer);
                                }
                            }
                            $scope.game.events.splice(0, 0, event);
                        }

                        if (event['type'] === "stat") {
                            updateChart([$scope.game.home.name, $scope.game.away.name], event['extra']);
                        }
                    });

                };

                eventList.start = function ()
                {
                    $scope.game.events = [];
                    $scope.game.status = 1;
                    webSocket.send(JSON.stringify($scope.game));
                };

                eventList.pause = function ()
                {
                    $scope.game.status = -1;
                    webSocket.send(JSON.stringify($scope.game));
                };
                
                eventList.params = function ()
                {
                    webSocket.send(JSON.stringify($scope.game));
                };

                eventList.init = function ()
                {
                    $scope.game.status = 0;
                    webSocket.send(JSON.stringify($scope.game));
                };

                eventList.resume = function ()
                {
                    $scope.game.status = 1;
                    webSocket.send(JSON.stringify($scope.game));
                };

                eventList.description = function (thiz) {
                    switch (thiz.action) {
                        case "D1P":
                            return "KICK OFF! Here we go for " + $scope.game.home.name + " " + $scope.game.away.name;
                        case "F1P" :
                            return "Half time! " + $scope.game.home.name + " " + eventList.score() + " " + $scope.game.away.name;
                        case "D2P" :
                            return "The second period between " + $scope.game.home.name + " and " + $scope.game.away.name + " has began!";
                        case "F2P":
                            return "Full time! " + $scope.game.home.name + " " + eventList.score() + " " + $scope.game.away.name;
                        case "TIR":
                            return "Shoot by " + thiz.player + " for " + thiz.team;
                        case "CJA":
                            return thiz.player + " is booked for a fault!";
                        case "CRO":
                            return thiz.player + " is sent off the game!";
                        case "BUT":
                            return "GOAL!!!! " + $scope.game.home.name + " " + eventList.score() + " " + $scope.game.away.name;
                        case "CGT":
                            return thiz.player + " is coming on for " + thiz.related;
                        default:
                            return thiz.action;
                    }
                };

                openSocket(eventList);
            });

});

function updateChart(labels, data) {
    var ctx = $("#myChart");
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: labels,
            datasets: [{
                    label: '# of Votes',
                    data: [data[labels[0]], data[labels[1]]],
                    backgroundColor: [
                        "#FF6384",
                        "#36A2EB"
                    ],
                }]
        }
    });
}

function openSocket(eventList) {
    // Ensures only one connection is open at a time
    if (webSocket !== undefined && webSocket.readyState !== WebSocket.CLOSED) {
        writeResponse("WebSocket is already opened.");
        return;
    }
    // Create a new instance of the websocket
    webSocket = new WebSocket("ws://" + location.host + "/socket/echo");

    /**
     * Binds functions to the listeners for the websocket.
     */
    webSocket.onopen = function (event) {
        // For reasons I can't determine, onopen gets called twice
        // and the first time event.data is undefined.
        // Leave a comment if you know the answer
        if (event.data === undefined) {
            eventList.init();
        }
        return;

        //writeResponse(event.data);
    };

    webSocket.onmessage = function (event) {
        console.log(event.data);
        eventList.add(event.data);
    };

    webSocket.onclose = function (event) {
        writeResponse("Connection closed");
    };
}


function send(message) {
    webSocket.send(message);
}

function closeSocket() {
    webSocket.close();
}

function writeResponse(text) {
    messages.innerHTML += "<br/>" + text;

}