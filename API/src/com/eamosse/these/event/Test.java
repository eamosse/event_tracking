/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event;


/**
 *
 * @author aedouard
 */
import java.util.Arrays;
import org.python.core.*;
import org.python.util.PythonInterpreter;

class Test {

    public static void main(String[] args) {
        Integer[] arr = {1,3,4,5,4};
        System.out.println(Arrays.toString(Arrays.copyOfRange(arr, arr.length-6, arr.length)));
    }

    private static void test2() {
        String text = "It's not all guts and glory. We still have to follow process (which is killer) and meet our milestones. Those can be grueling, especially when that giant dude with the bad metal teeth starts lurking around right before deadline. The benefits plan is amazing though � free travel around the world, staying at some of the most fantastics resorts. Overall, serving evil a pretty good deal.";
        PythonInterpreter interpreter = new PythonInterpreter();
        interpreter.exec("from helper.textrank import *");
        PyInstance obj = (PyInstance)interpreter.eval("extractSentences('"+text+"')");
        System.out.println(obj.invoke("get_name").__tojava__(String.class));
    }
}
