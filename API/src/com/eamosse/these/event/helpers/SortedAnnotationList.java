/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.helpers;

import com.eamosse.these.event.models.MAnnotation;
import com.eamosse.these.event.models.Tweet;
import gate.Annotation;
import gate.AnnotationSet;
import gate.Document;
import gate.util.InvalidOffsetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aedouard
 */
public class SortedAnnotationList extends Vector<MAnnotation> {

    Document document;
    AnnotationSet annotations;
    String[] types = new String[]{"tweet", "text", "created_at", "id"};
    HashMap<String, Tweet> tweetMap;
    SortedAnnotationList sortedAnnotations;// = new SortedAnnotationList();
    public int nbTweets = 0;

    public SortedAnnotationList() {
        super();
        //Iterator<String> iterator = new ArrayList<String>().iterator(); 
    }

    public SortedAnnotationList(Document document) {
        super();
        this.document = document;
        this.annotations = document.getNamedAnnotationSets().get("Original markups");
        //annotations.iterator()
        this.indexOriginalMarkups();
    } // SortedAnnotationList

    final void indexOriginalMarkups() {
        sortedAnnotations = new SortedAnnotationList();
        sortedAnnotations.annotations = annotations;
        annotations.get(new HashSet<>(Arrays.asList(types))).stream().map((annotation) -> new MAnnotation(annotation)).forEach((annotation1) -> {
            sortedAnnotations.addSortedExclusive(annotation1, false);
        }); 
        nbTweets = sortedAnnotations.size();
    }

    String getContent(Annotation a) {
        try {
            return document.getContent().getContent(a.getStartNode().getOffset(), a.getEndNode().getOffset()).toString();
        } catch (InvalidOffsetException ex) {
            Logger.getLogger(SortedAnnotationList.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    void annotationMapping(MAnnotation annot) {
         //System.out.println("Creating tweet " + annot);
         //System.out.println(sortedAnnotations);
        sortedAnnotations.stream().filter((annotation) -> (annot.parent.withinSpanOf(annotation.parent))).map((annotation) -> {
            //get the tweet annotation
            if (tweetMap == null) {
                tweetMap = new HashMap<>();
            }
            return annotation;
        }).forEach((annotation) -> {
            if (tweetMap.containsKey(getContent(annotation.findChild("id").parent))) {
                //System.out.println("Creating tweet contains" + annotation);
                annot.tweet = tweetMap.get(getContent(annotation.findChild("id").parent));
                //System.out.println("Found " + annot.tweet);
            } else {
                //System.out.println("Creating tweet not contains " + annotation);
                Tweet t = new Tweet();
                t.created_at = getContent(annotation.findChild("created_at").parent);
                t.text = getContent(annotation.findChild("text").parent);
                t.id = getContent(annotation.findChild("id").parent);
                annot.tweet = t;
                tweetMap.put(getContent(annotation.findChild("id").parent), t);
                //System.out.println("New Add " + annot.tweet);
            }
        }); //annotations.get(new HashSet<>(Arrays.asList(types)))) {
    }

    @Override
    public synchronized String toString() {
        return super.toString(); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean addSortedExclusive(MAnnotation annot, boolean mapping) {
        MAnnotation currAnot = null;
        boolean hasFound = false;
        
        // overlapping check
        for (int i = 0; i < size(); ++i) {
            currAnot = (MAnnotation) get(i);
            if (annot.parent.withinSpanOf(currAnot.parent)) {
                currAnot.child.add(annot);
                currAnot.sortChildren();
                return false;
            } else {
                if (currAnot.parent.withinSpanOf(annot.parent)) {
                    annot.child.add(currAnot);
                    annot.tweet = currAnot.tweet;
                    if (!hasFound) {
                        this.add(i, annot);
                        this.removeElementAt(i + 1);
                    } else {
                        this.removeElementAt(i);
                    }
                    hasFound = true;
                }
            }
        } // for
        
        
        
        if (hasFound) {
            annot.sortChildren();
            return false;
        }

        if (mapping) {
            annotationMapping(annot);
        }
        long annotStart = annot.parent.getStartNode().getOffset();
        long currStart;

        // insert
        for (int i = 0; i < size(); ++i) {
            currAnot = (MAnnotation) get(i);
            currStart = currAnot.parent.getStartNode().getOffset();
            if (annotStart < currStart) {
                insertElementAt(annot, i);
                /*
                 Out.prln("Insert start: "+annotStart+" at position: "+i+" size="+size());
                 Out.prln("Current start: "+currStart);
                 */
                return true;
            } // if
        } // for

        int size = size();
        insertElementAt(annot, size);
        return true;
    } // addSorted
} // SortedAnnotationList
