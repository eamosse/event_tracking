/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event;

import com.eamosse.these.event.enums.ParticipantType;
import com.eamosse.these.event.graph.JGraphManager;
import com.eamosse.these.event.models.MAnnotation;
import com.eamosse.these.event.models.Participant;
import com.eamosse.these.event.helpers.SortedAnnotationList;
import com.eamosse.these.event.models.Action;
import com.eamosse.these.event.models.Player;
import com.eamosse.these.event.models.Team;
import com.eamosse.these.event.server.EventTrackingServer;
import com.eamosse.these.event.utils.AppUtils;
import gate.Annotation;
import gate.AnnotationSet;
import gate.Document;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

/**
 *
 * @author aedouard
 */
public class ActionParser {

    static String[] annotationFeatures = new String[]{"ACTION", "SUBT", "Lookup"};
    static Set<Participant> sets;
    //static String action;
    static JGraphManager graph;

    public static void write(String text) {
        BufferedWriter bw = null;
        FileWriter fw = null;

        try {


            fw = new FileWriter("result.xml");
            bw = new BufferedWriter(fw);
            bw.write(text);

            System.out.println("Done");

        } catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {

                if (bw != null) {
                    bw.close();
                }

                if (fw != null) {
                    fw.close();
                }

            } catch (IOException ex) {

                ex.printStackTrace();

            }

        }
    }

    public static void parse(AnnieHelper annie) {

        EventTrackingServer.getInstance().stats = new HashMap<>();
        Stream<Document> stream = annie.getCorpus().stream().map((doc) -> {
            return doc;
        });
        if (stream == null) {
            return;
        }
        try {
            stream.forEach((Document doc) -> {
                //write(doc.toXml());
                AnnotationSet defaultAnnotSet = doc.getAnnotations();
                Set annotTypesRequired = new HashSet();
                annotTypesRequired.addAll(Arrays.asList(annotationFeatures));
                Set<Annotation> annotations = new HashSet<>(defaultAnnotSet.get(annotTypesRequired));
                if (!(annotations.isEmpty())) {
                    SortedAnnotationList _sortedAnnotations = new SortedAnnotationList(doc);
                    annotations.stream().map((annotation) -> new MAnnotation(annotation)).forEach((annotation1) -> {
                        _sortedAnnotations.addSortedExclusive(annotation1, true);
                    });

                    Set<MAnnotation> sortedAnnotations = new LinkedHashSet<>(_sortedAnnotations);

                    sortedAnnotations.stream().forEach((MAnnotation mAnnotation) -> {
                        Annotation annotation = mAnnotation.parent;

                        if (mAnnotation.tweet != null) {
                            HashMap<String, Set<String>> features = new HashMap<>();
                            //System.out.println(annotation);
                            sets = new HashSet<>();
                            switch (annotation.getType()) {
                                case "ACTION":

                                    //System.out.println(annotation);
                                    annotation.getFeatures().keySet().stream().forEach((key) -> {

                                        if (key.toString().contains("action")) {
                                            mAnnotation.action = annotation.getFeatures().get(key).toString();
                                        } else {
                                            add(key.toString(), annotation.getFeatures().get(key).toString());
                                        }
                                    });
                                    if (!mAnnotation.action.equals("N/A")) {
                                        mAnnotation.tweet.addAction(mAnnotation.action, sets);
                                    }
                                    break;

                                case "SUBT":
                                    ArrayList<Participant> participant_in = new ArrayList<>();
                                    ArrayList<Participant> participant_out = new ArrayList<>();
                                    mAnnotation.action = "CGT";//annotation.getFeatures().get("action").toString();
                                    if (annotation.getFeatures().get("player_in") != null || annotation.getFeatures().get("player_out") != null) {
                                        try {
                                            String[] annot_in = annotation.getFeatures().get("player_in") != null ? annotation.getFeatures().get("player_in").toString().split(";") : new String[0];
                                            String[] annot_out = annotation.getFeatures().get("player_out") != null ? annotation.getFeatures().get("player_out").toString().split(";") : new String[0];
                                            for (String in : annot_in) {
                                                participant_in.add(computeState("player", in));
                                            }
                                            for (String in : annot_out) {
                                                participant_out.add(computeState("player", in));
                                            }
                                            if (annot_in.length == annot_out.length) {
                                                for (int i = 0; i < annot_in.length; i++) {
                                                    Participant p = participant_in.get(i);
                                                    Participant pp = participant_out.get(i);
                                                    p.related = pp;
                                                    sets.add(p);
                                                }
                                                mAnnotation.tweet.addAction(mAnnotation.action, sets);
                                            } else {
                                                participant_in.forEach((par) -> {
                                                    sets.add(par);
                                                });
                                                mAnnotation.action = "CGT_IN";
                                                if (!sets.isEmpty()) {
                                                    mAnnotation.tweet.addAction(mAnnotation.action, sets);
                                                    sets = new HashSet<>();
                                                }

                                                participant_out.forEach((par) -> {
                                                    sets.add(par);
                                                });
                                                mAnnotation.action = "CGT_OUT";
                                                if (!sets.isEmpty()) {
                                                    mAnnotation.tweet.addAction(mAnnotation.action, sets);
                                                }
                                            }

                                        } catch (Exception ex) {
                                            // ex.printStackTrace();
                                            // Logger.getLogger(ActionParser.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                    }
                                    break;
                                case "Lookup":
                                    Optional<Object> filter = annotation.getFeatures().keySet().stream().filter(key -> key.equals("minorType")).findFirst();
                                    if (filter.isPresent()) {
                                        String minorType = (String) filter.get();
                                        switch (minorType) {
                                            case "country":
                                            case "player":
                                                add(minorType, annotation.getFeatures().get(minorType).toString());
                                                break;
                                            case "action":
                                                mAnnotation.action = annotation.getFeatures().get("majporType").toString();
                                                break;
                                        }
                                    }
                                    if (!mAnnotation.action.equals("N/A")) {
                                        mAnnotation.tweet.addAction(mAnnotation.action, sets);
                                    }
                                    break;
                            }

                        }
                    });

                    graph = new JGraphManager();
                    Stream<MAnnotation> filteredAnnotations = sortedAnnotations.stream().filter((mAnnotation) -> (mAnnotation.tweet != null && !mAnnotation.tweet.getActions().keySet().isEmpty())).map((mAnnotation) -> {
                        return mAnnotation;
                    });
                    filteredAnnotations.forEach((mAnnotation) -> {
                        if (mAnnotation.action.equals("TIR")) {
                            System.out.println(mAnnotation.tweet.id);
                        }
                        graph.addToGraph(mAnnotation.tweet.text, mAnnotation.tweet.getActions());
                    });
                    //double total = sortedAnnotations.size();
                    EventTrackingServer.getInstance().gameStats.add((double) graph.actionTweets);

                    index = 0;
                    double[] gameValues = AppUtils.toDouble(EventTrackingServer.getInstance().gameStats);
                    HashMap<String, Double> statGame = AppUtils.computeStat(gameValues, sortedAnnotations.size());

                    EventTrackingServer.getInstance().stats.keySet().forEach((key) -> {
                        ArrayList<Double> get = EventTrackingServer.getInstance().teamStats.getOrDefault(key, new ArrayList<>());
                        get.add(EventTrackingServer.getInstance().stats.get(key));
                        EventTrackingServer.getInstance().teamStats.put(key, get);
                    });

                    EventTrackingServer.getInstance().teamStats.keySet().forEach(key -> {
                        double[] teamValues = AppUtils.toDouble(EventTrackingServer.getInstance().teamStats.get(key)); //.size()];
                        HashMap<String, Double> stat = AppUtils.computeStat(teamValues, EventTrackingServer.getInstance().stats.get(key));
                        EventTrackingServer.getInstance().stats.put(key, stat.get("teta"));// / statGame.get("pound"));
                        EventTrackingServer.getInstance().gameRatio = statGame.get("teta");
                    });

                    //System.out.println("TEAM" + EventTrackingServer.getInstance().teamStats);
                    //System.out.println("GAME" + EventTrackingServer.getInstance().stats);
                    //System.out.println(StreamAnalyzer.stats);
                    graph.nbTweets = sortedAnnotations.size();
                    graph.compute();
                    ArrayList<Action> actions = graph.footActions;
                    EventAnalyzer.analyze(actions, graph.nbTweets);
                    EventAnalyzer.tweets.add(graph.nbTweets);
                    
                    /*
                    actions.stream().filter((action) -> (!action.participants.isEmpty())).forEachOrdered((action) -> {
                        action.performNotification();
                        action.deleteObservers();
                    });
                    */

                }
            });
        } catch (Exception exp) {
            System.out.println("Something went wrong with this document....");
        }
        annie.cleanup();
        System.gc();
    }
    static int index;

    static Participant computeState(String key, String annotation) throws Exception {

        Participant participant = Participant.initFromFeatures(key, annotation);
        double val = 0;
        if (participant != null && participant.type == ParticipantType.PLAYER) {
            Team team = ((Player) participant.value).team;
            if (EventTrackingServer.getInstance().stats.containsKey(team)) {
                val = EventTrackingServer.getInstance().stats.get(team);
            }
            EventTrackingServer.getInstance().stats.put(team, val + 1);
        } else {
            if (participant.type == ParticipantType.TEAM) {
                Team team = ((Team) participant.value);
                if (EventTrackingServer.getInstance().stats.containsKey(team)) {
                    val = EventTrackingServer.getInstance().stats.get(team);
                }
                EventTrackingServer.getInstance().stats.put(team, val + 1);
            }
        }
        return participant;
    }

    static void add(String key, String annotation) {
        try {
            sets.add(computeState(key, annotation));

        } catch (Exception ex) {
            //Logger.getLogger(ActionParser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
