/*
 *  StandAloneAnnie.java
 *
 *
 * Copyright (c) 2000-2001, The University of Sheffield.
 *
 * This file is part of GATE (see http://gate.ac.uk/), and is free
 * software, licenced under the GNU Library General Public License,
 * Version 2, June1991.
 *
 * A copy of this licence is included in the distribution in the file
 * licence.html, and is also available at http://gate.ac.uk/gate/licence.html.
 *
 *  hamish, 29/1/2002
 *
 *  $Id: StandAloneAnnie.java,v 1.6 2006/01/09 16:43:22 ian Exp $
 */
package com.eamosse.these.event;

import com.eamosse.these.event.manager.TeamManager;
import com.eamosse.these.event.models.ActionResult;
import com.eamosse.these.event.models.Game;
import com.eamosse.these.event.models.Event;
import com.eamosse.these.event.mongodb.DBManager;
import com.eamosse.these.event.mongodb.JsonHelper;
import com.eamosse.these.event.server.ActionCallback;
import com.eamosse.these.event.server.EventTrackingServer;
import com.eamosse.these.event.utils.PropertiesHelper;
import java.util.*;
import java.io.*;
import gate.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class illustrates how to use ANNIE as a sausage machine in another
 * application - put ingredients in one end (URLs pointing to documents) and get
 * sausages (e.g. Named Entities) out the other end.
 * <P>
 * <B>NOTE:</B><BR>
 * For simplicity's sake, we don't do any exception handling.
 */
public class StreamAnalyzer {

    private static StreamAnalyzer INSTANCE;// = new EventTrackingServer();
    public Game game;

    public static StreamAnalyzer getInstance() {
        synchronized (EventTrackingServer.class) {
            if (INSTANCE == null) {
                PropertiesHelper.loadKey();
                System.setProperty("-Dgate.plugins.home", PropertiesHelper.GATE_HOME + "/plugins");
                System.setProperty("gate.home", PropertiesHelper.GATE_HOME);
                INSTANCE = new StreamAnalyzer();
            }
        }
        return INSTANCE;
    }

    EventTrackingServer kkp;
    boolean stop = false;

    public ArrayList<ActionCallback> callbacks = new ArrayList<>();

    public static File[] loadFiles(String folder) {
        File f = new File(folder);
        File[] files = f.listFiles();

        Arrays.sort(files, (File o1, File o2) -> {
            if (!o2.getName().contains(".xml")) {
                return 1;
            }
            int p1 = Integer.parseInt(o1.getName().replace(".xml", "").split("_")[1]);
            int p2 = Integer.parseInt(o2.getName().replace(".xml", "").split("_")[1]);
            return p1 > p2 ? 1 : -1;
        });
        return files;
    }

    public static void main(String args[]) throws GateException, IOException {
        if (args.length < 4) {
            args = new String[]{
                "England", "Wales", "13", "/user/aedouard/home/GATE/", "0.25"};
        }
        StreamAnalyzer an = StreamAnalyzer.getInstance();
        an.game = new Game(TeamManager.findByKey("nicknames", new String[]{args[0].toLowerCase()}), TeamManager.findByKey("nicknames", new String[]{args[1].toLowerCase()}), Integer.parseInt(args[2]));
        
        an.callbacks.add(new ActionCallback() {
            @Override
            public void onResult(Event action) {
                ActionResult res = action.toResult();
                System.out.println(res);
            }

            @Override
            public void onIteration(Object object) {
                //System.out.println(JsonHelper.createJsonObject(object));
            }
        });

        an.run();

    }

    public void stop() {
        stop = true;
        kkp.stop = true;
        game.status = 0;
    }
    int current_time = 0, current_hour = 0;
    int done = 0;

    public void resume() {
        stop = false;
        kkp.stop = false;
        game.status = 1;
        run();
    }

    public void restart() {
        stop = false;
        done = 0;
        game.status = 1;
        current_time = 0;
        current_hour = 0;
        if (kkp != null) {
            kkp.deInit();
        }
        run();
    }

    public void run() {
        try {
            kkp = EventTrackingServer.getInstance();
            kkp.trackGame(game);
            int hour = current_hour == 0 ? game.start : current_hour;
            String collectionName = game.home + "_" + game.away;
            ArrayList<File> documents = new ArrayList<>();

            for (int h = hour; h <= hour + 1; h++) {
                kkp.gamePeriod++;
                current_time = current_time < 60 ? current_time : 0;
                while (current_time < 60 && !stop) {
                    File current_file = DBManager.findTweetsPerInterval(collectionName, h, current_time, current_time + kkp.windowInterval);
                    System.out.println("Processing > " + current_file.getName());
                    documents.add(current_file);
                    kkp.observer.newBatch();
                    System.out.println("Processing....");
                    kkp.processInput(documents, callbacks);
                    System.out.println("End processing...");
                    current_time += kkp.windowInterval;
                    kkp.onIteration(null);
                    System.gc();
                }
                if (stop) {
                    break;
                }
                done++;
            }
            if (done == 2) {
                done = 0;
                current_time = 0;
                current_hour = 0;
                kkp.deInit();
            }
        } catch (GateException ex) {
            Logger.getLogger(StreamAnalyzer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

} // class StandAloneAnnie
