/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event;

/**
 *
 * @author aedouard
 */
public class MRunner {

    public String home, away;
    public String time;

    public MRunner(String home, String away, int time) {
        this.home = home;
        this.away = away;
        this.time = String.valueOf(time);
    }
}
