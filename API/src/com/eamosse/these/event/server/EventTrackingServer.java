/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.server;

import com.eamosse.these.event.ActionParser;
import com.eamosse.these.event.AnnieHelper;
import com.eamosse.these.event.StreamAnalyzer;
import com.eamosse.these.event.models.Game;
import com.eamosse.these.event.models.Event;
import com.eamosse.these.event.models.Team;
import com.eamosse.these.event.observers.ActionObserver;
import com.eamosse.these.event.observers.EventObserver;
import com.eamosse.these.event.utils.AppUtils;
import com.eamosse.these.event.utils.PropertiesHelper;
import gate.Gate;
import gate.util.GateException;
import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EventTrackingServer implements ActionCallback {

    ArrayList<ActionCallback> callbacks;
    public EventObserver actionObserver;
    public ActionObserver observer;
    public int windowInterval = 2;
    public int gameStatus = 0;
    public int gamePeriod = 0;
    public double gameRatio = 0.01;
    public Game game;
    public HashMap<Team, Double> stats;
    public HashMap<Team, ArrayList<Double>> teamStats;
    public ArrayList<Double> gameStats;
    public boolean stop = false;
    boolean isInit = false;

    public static EventTrackingServer getInstance() {
        synchronized (EventTrackingServer.class) {
            if (INSTANCE == null) {
                INSTANCE = new EventTrackingServer();
            }
        }
        return INSTANCE;
    }

    private static EventTrackingServer INSTANCE;// = new EventTrackingServer();

    public void trackGame(Game game) throws GateException {
        Gate.init();
        this.game = game;
        System.out.println(PropertiesHelper.GATE_HOME);
        AppUtils.emptyFolder("data");
        AppUtils.deleteIt(game.home + "_" + game.away + ".text");
        actionObserver = new EventObserver();
        observer = new ActionObserver();
        teamStats = new HashMap<>();
        stats = new HashMap<>();
        gameStats = new ArrayList<>();
        isInit = true;
        stop = false;
    }

    void initAnnie() throws GateException, MalformedURLException {
        annie = new AnnieHelper();
        annie.initAnnie();
    }

    public void deInit() {
        if (!isInit) {
            return;
        }
        isInit = false;
        annie.cleanup();
        annie = null;
        actionObserver.deInit();
        observer.deInit();
        actionObserver = null;
        observer = null;
        stats = null;
        teamStats = null;
        gameStats = null;
        System.gc();
    }

    void runAnni(ArrayList<File> documents) {
        try {
            if (annie == null) {
                initAnnie();
            }
            System.out.println("exec annie");
            annie.execute(documents);
            System.out.println("annie executed...");
            ActionParser.parse(annie);
            System.out.println("annied parsed");
            documents.clear();
        } catch (GateException | MalformedURLException ex) {
            Logger.getLogger(StreamAnalyzer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    AnnieHelper annie;

    public void processInput(ArrayList<File> files, ArrayList<ActionCallback> callbacks) {
        this.callbacks = callbacks;
        runAnni(files);
    }

    @Override
    public void onResult(Event action) {
        if (!stop) {
            StreamAnalyzer.getInstance().game.events.add(action.toResult());
            callbacks.forEach((callback) -> {
                callback.onResult(action);
            });

        }
    }

    @Override
    public void onIteration(Object object) {
        HashMap<String, Double> maps = new HashMap<>();
        Event e = new Event();
        e.type = "stat";
        teamStats.keySet().forEach((key) -> {
            maps.put(key.name, teamStats.get(key).stream().reduce((a, b) -> a + b).get());
        });
        e.extra = maps;

        callbacks.forEach((callback) -> {
            callback.onIteration(e);
        });
    }

}
