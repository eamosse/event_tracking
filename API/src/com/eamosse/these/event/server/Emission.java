/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.server;

import java.io.PrintWriter;
import java.util.Scanner;

public class Emission implements Runnable {

    private PrintWriter out;
    private String message = null;
    private Scanner sc = null;

    public Emission(PrintWriter out) {
        this.out = out;
    }

    @Override
    public void run() {
        sc = new Scanner(System.in);
        out.println("welcome :)");
        out.flush();
        while (true) {
            System.out.println("Votre message :");
            message = sc.nextLine();
            out.println(message);
            out.flush();
        }
    }

}
