/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.server;

import com.eamosse.these.event.models.Event;

/**
 *
 * @author aedouard
 */
public interface ActionCallback {
    public void onResult(Event action);
    public void onIteration(Object object);
}
