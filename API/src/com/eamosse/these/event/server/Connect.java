/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.server;

import java.io.*;
import java.net.*;

public class Connect implements Runnable {

    private ServerSocket socketserver = null;
    private Socket client = null;
    
    final private SocketConnect callback; 

    public Thread t1;

    public Connect(ServerSocket ss, SocketConnect callback)
    {
        this.callback = callback; 
        socketserver = ss;
    }

    @Override
    public void run() {

        try {
            while (true) {
                client = socketserver.accept();
                System.out.println("Nouvelle connexion...");
                callback.onConnect(client);

                Thread t3 = new Thread(new Reception(new BufferedReader(new InputStreamReader(client.getInputStream()))));
                t3.start();

                Thread t = new Thread(new Emission(new PrintWriter(client.getOutputStream())));
                t.start();
            }
        } catch (IOException e) {
            System.err.println("Erreur serveur");
        }

    }
}
