/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.server;

import java.net.Socket;

/**
 *
 * @author aedouard
 */
public interface SocketConnect {
    public void onConnect(Socket socket);
}
