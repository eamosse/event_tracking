/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.models;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author aedouard
 */
@XmlRootElement
public class Tweets {

    public Tweets() {
        this.tweet = new ArrayList<>();
    }

    public List<Tweet> tweet;

    public Tweets(List<Tweet> tweets) {
        this.tweet = tweets;
    }

    public void addTweet(Tweet tweet) {
        this.tweet.add(tweet);
    }

    public File toXml(String fileName) {
        try {
            File temp = new File("data/"+fileName);
            if (tweet.isEmpty()) {
                System.out.println("How comes it is empty");
            }

            JAXBContext jaxbContext = JAXBContext.newInstance(Tweets.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            FileWriter sw = new FileWriter(temp);
            jaxbMarshaller.marshal(this, sw);
            return temp;
            
        } catch (JAXBException | IOException ex) {
            Logger.getLogger(Tweets.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return null;
    }
}
