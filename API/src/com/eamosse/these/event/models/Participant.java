/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.models;

import com.eamosse.these.event.enums.ParticipantType;
import com.eamosse.these.event.StreamAnalyzer;
import com.eamosse.these.event.manager.PlayerManager;
import com.eamosse.these.event.manager.TeamManager;
import com.eamosse.these.event.server.EventTrackingServer;
import java.util.Arrays;
import java.util.Objects;

/**
 *
 * @author aedouard
 */
public class Participant<V> {

    public ParticipantType type;
    public V value;
    public Participant<V> related;

    public Participant(ParticipantType type, V value) {
        this.type = type;
        this.value = value;
        this.related = null;
    }

    public static Participant initFromFeatures(String type, String value) throws Exception {
        value = value.replace("#", "");//.toLowerCase();
        if (type.startsWith("team")) {
            Team team = TeamManager.findByKey("nicknames", new String[]{value.toLowerCase()});
            if (team != null && Arrays.asList(new String[] {EventTrackingServer.getInstance().game.home.name, EventTrackingServer.getInstance().game.away.name}).contains(team.name)) {
                //value = team.name;
                return new Participant(ParticipantType.TEAM, team);
            } else {
                throw new Exception(value + " is a wrong team");
            }
        }

        if (type.startsWith("player")) {
            Player player = PlayerManager.findByKey("nicknames", new String[]{value.toLowerCase()});
            if (player != null && Arrays.asList(new String[] {EventTrackingServer.getInstance().game.home.name, EventTrackingServer.getInstance().game.away.name}).contains(player.team.name)) {
                return new Participant(ParticipantType.PLAYER, player);
                //participants.add(new Participant(ParticipantType.TEAM, player));
            } else {
                throw new Exception(value + " is a wrong player");
            }
        }
        if (type.contains("result")) {
            return new Participant(ParticipantType.RESULT, value);
        }
        return null;
    }

    @Override
    public String toString() {
        return value.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Participant)) {
            return false;
        }
        Participant other = (Participant) obj;
        return this.type.equals(other.type) && other.value.equals(this.value);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.type);
        hash = 79 * hash + Objects.hashCode(this.value);
        return hash;
    }

}
