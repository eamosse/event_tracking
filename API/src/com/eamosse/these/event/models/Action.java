/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.models;

import com.eamosse.these.event.enums.ParticipantType;
import com.eamosse.these.event.graph.RelationshipEdge;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Objects;
import java.util.Observable;
import java.util.Set;

/**
 *
 * @author aedouard
 */
public class Action extends Observable {

    public String type;
    public int actionTweets = 0;
    
    public Set<ActionParticipant> participants = new HashSet<>();

    public Action(int actionTweets) {
        this.actionTweets = actionTweets;
    }

    public void addRelation(RelationshipEdge relationshipEdge) {
        if (relationshipEdge == null) {
            return;
        }
        this.type = relationshipEdge.getAction().toString(); //.toLowerCase();
        participants.add(new ActionParticipant(relationshipEdge));
        /*if (relationshipEdge.label.equals(ParticipantType.TIME.name().toLowerCase())) {
            actionTweets = relationshipEdge.nb;
        }*/
    }

    public void mergeParticipant(Set<ActionParticipant> participants1) {
        participants.forEach((ActionParticipant actionParticipant) -> {
            participants1.stream().filter((actionParticipant1) -> (actionParticipant.participant.type.equals(actionParticipant1.participant.type))).map((participant1) -> {
                if (actionParticipant.participant.type.equals(ParticipantType.TIME)) {
                    actionParticipant.participant = participant1.participant;
                }
                return participant1;
            }).forEachOrdered((participant1) -> {
                actionParticipant.confidence += participant1.confidence;
                // notif(participant);
            });
        });
    }

    public String getValueForParticipant(ParticipantType type) {
        ActionParticipant p = getParticipantWithType(type);
        if (p != null) {
            return p.participant.value.toString();
        }
        return null;
    }

    public ActionParticipant getParticipantWithType(ParticipantType type) {
        for (ActionParticipant p : participants) {
            if (p.participant.type.equals(type)) {
                return p;
            }
        }
        return null;
    }

    public void removeParticipant(ActionParticipant participant) {
        //System.out.println("To Remove " + participant + " " +participants);
        //participants.remove(participant);
        participants = new HashSet<>();
        //System.out.println("Removed " + participants);

    }

    public boolean hasParticipant(Action ap, ParticipantType type) {
        String p1 = getValueForParticipant(type);
        String p2 = ap.getValueForParticipant(type);
        return p1 != null && p2 != null && p1.equalsIgnoreCase(p2);
    }

    public void cleanParticipants() {
        ArrayList<ActionParticipant> lists = new ArrayList<>(participants);
        ArrayList<ActionParticipant> lists2 = new ArrayList<>(participants);
        for (int i = 0; i < lists2.size(); i++) {
            for (int j = i + 1; j < lists2.size(); j++) {
                if (lists2.get(i).participant.equals(lists2.get(j).participant)) {
                    if (lists2.get(i).confidence > lists2.get(j).confidence) {
                        lists.get(i).confidence += lists2.get(j).confidence;
                        lists.remove(j);
                    } else {
                        lists.get(j).confidence += lists2.get(i).confidence;
                        lists.get(j).confidence += lists2.get(i).confidence;
                        lists.remove(i);
                    }
                }
            }
        }
        participants = new HashSet<>(lists);
    }

    public void performNotification() {
        if (this.getParticipantWithType(ParticipantType.TIME) != null) {
            setChanged();
            notifyObservers(actionTweets);
        } else {
            System.out.println("Weired though " + this);
        }

    }

    @Override
    public String toString() {
        return type + " " + participants  + " " + actionTweets;
    }

    public boolean hasSameParticipant(Action other) {
        ActionParticipant p1 = this.getParticipantWithType(ParticipantType.PLAYER);
        ActionParticipant p2 = other.getParticipantWithType(ParticipantType.PLAYER);
        return p1 != null && p2 != null && p1.equals(p2);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Action)) {
            return false;
        }
        Action other = (Action) obj;
        if (!other.type.equals(this.type)) {
            return false;
        }
        //same player 
        ActionParticipant p1 = this.getParticipantWithType(ParticipantType.PLAYER);
        ActionParticipant p2 = this.getParticipantWithType(ParticipantType.PLAYER);
        return p1 != null && p2 != null && p1.equals(p2);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.type);
        return hash;
    }

}
