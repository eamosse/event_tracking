/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.models;

import java.util.ArrayList;

/**
 *
 * @author aedouard
 */
public class ActionResult {
    public String action, time, player, team, related, score, resume;
    public ArrayList<String> tweets;

    @Override
    public String toString() {
        return time + " " + action + " " + player;
    }
    
    
}
