/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.models;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author aedouard
 */
public class Team {
    
    public String name,code,id,crestUrl; 
    public ArrayList<String> nicknames, scorers ; 

    public Team(String name) {
        this.name = name;
    }

    public Team() {
        scorers = new ArrayList<>();
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.name);
        hash = 67 * hash + Objects.hashCode(this.code);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Team other = (Team) obj;
        return other.name.equals(this.name);
    }
    
    
    
    
}
