/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.models;

import java.util.Objects;

/**
 *
 * @author aedouard
 */
public class Parameter {

    public String key;
    public double value, ratio;

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Parameter)) {
            return false;
        }
        Parameter other = (Parameter)obj; 
        return other.key.equalsIgnoreCase(other.key);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.key);
        return hash;
    }

}
