/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.models;

import com.eamosse.these.event.utils.Constants;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import org.apache.commons.lang.ArrayUtils;

/**
 *
 * @author aedouard
 */
public class Game {

    public Team home, away; 
    private String name;
    public int start;
    public String date;

    public Set<Parameter> parameters;

    public String getName() {
        return home.name + " VS " + away.name;
    }
    String[] periods = {"D1P", "D2P", "F1P", "F2P"};
    public int status = 0;
    public ArrayList<ActionResult> events = new ArrayList<>();

    public Game(Team home, Team away, int start) {
        this.home = home;
        this.away = away;
        this.start = start;
        this.name = home + " VS " + away;
        initParam();
    }

    private void initParam() {
        parameters = new HashSet<>();
        Constants.actionPounds.keySet().stream().map((key) -> {
            Parameter p = new Parameter();
            p.key = ArrayUtils.contains(periods, key) ? "Period" : key;
            p.value = Constants.actionPounds.get(key) / 10;
            p.ratio = Constants.actionPounds.get(key);
            return p;
        }).forEachOrdered((p) -> {
            parameters.add(p);
        });
    }

    public Game() {
        initParam();
    }

    public double thresholedForAction(String key) {
        if (parameters == null || parameters.isEmpty()) {
            System.out.println("Parameter is nul????? oooo");
            initParam();
        }
        Optional<Parameter> filter = this.parameters.stream().filter((p) -> p.key.equals(ArrayUtils.contains(periods, key) ? "Period" : key)).findAny();
        if (filter.isPresent()) {
            return filter.get().value * 10;
        } else {
            return 1350.0;
        }
    }

}
