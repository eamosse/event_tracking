/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.models;

import com.eamosse.these.event.graph.RelationshipEdge;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author aedouard
 * @param <V>
 */
public class ActionParticipant<V> {

    // public String value;
    //public String label;
    public Participant<V> participant;
    public int confidence;
    public boolean isConfirmed;
    public String action;
    public Participant<V> related;
    public ArrayList<String> tweets = new ArrayList<>();

    public ActionParticipant() {
    }

    public ActionParticipant(RelationshipEdge relation) {
        this.participant = relation.getParticipant();
        this.action = relation.getAction().toString();
        this.confidence = relation.nb;
        this.tweets = relation.tweets;
        if (relation.related != null) {
            related = relation.related;
        }
    }

    @Override
    public String toString() {
        return participant.value + " " + confidence + " " + isConfirmed;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ActionParticipant)) {
            return false;
        }
        ActionParticipant other = (ActionParticipant) obj;
        return this.action.equals(other.action) && this.participant.equals(other.participant);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.participant);
        hash = 89 * hash + Objects.hashCode(this.action);
        return hash;
    }

}
