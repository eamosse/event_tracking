/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.models;

import com.eamosse.these.event.StreamAnalyzer;
import com.eamosse.these.event.enums.ActionStatus;
import com.eamosse.these.event.enums.ParticipantType;
import com.eamosse.these.event.server.EventTrackingServer;
import com.eamosse.these.event.utils.AppUtils;
import com.eamosse.these.event.utils.XmlParser;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Observable;

/**
 *
 * @author aedouard
 */
public class Event extends Observable {

    public String type, time, result, step;
    public ActionParticipant<Player> player;
    public Action action;
    public double pound;
    public int frequency;
    public int confirmedCount = 1, nbTweets, nbActionTweets, gameStatus = 1;
    public boolean allowedInPause = false, participantRequired = true, inGame = true;
    public ArrayList<String> tweets = new ArrayList<>();
    public String resume;
    public Object extra;
    public boolean isValid = false;

    public boolean isInGame() {
        return inGame;
    }

    public void setInGame(boolean inGame) {
        this.inGame = inGame;
    }

    public Event getRelated() {
        return related;
    }

    public void setRelated(Event related) {
        this.related = related;
    }
    public Event related;

    public int getNbActionTweets() {
        return nbActionTweets;
    }

    public void setNbActionTweets(int nbActionTweets) {
        this.nbActionTweets = nbActionTweets;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public int getConfirmedCount() {
        return confirmedCount;
    }

    public void setConfirmedCount(int confirmedCount) {
        this.confirmedCount = confirmedCount;
    }

    public int getNbTweets() {
        return nbTweets;
    }

    public void setNbTweets(int nbTweets) {
        this.nbTweets = nbTweets;
    }

    public int getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(int gameStatus) {
        this.gameStatus = gameStatus;
    }

    public boolean isAllowedInPause() {
        return allowedInPause;
    }

    public void setAllowedInPause(boolean allowedInPause) {
        this.allowedInPause = allowedInPause;
    }

    public boolean isParticipantRequired() {
        return participantRequired;
    }

    public void setParticipantRequired(boolean participantRequired) {
        this.participantRequired = participantRequired;
    }

    public Event(String type) {
        this.type = type;
        //isAllowed in pause 
        String attributeValue = XmlParser.valueForActionAttribute(type, "allowedInPause");
        if (attributeValue != null) {
            this.allowedInPause = Boolean.valueOf(attributeValue);
        }
        attributeValue = XmlParser.valueForActionAttribute(type, "gamestatus");
        if (attributeValue != null) {
            this.gameStatus = Integer.valueOf(attributeValue);
        }
        attributeValue = XmlParser.valueForActionAttribute(type, "participantRequired");
        if (attributeValue != null) {
            this.participantRequired = Boolean.valueOf(attributeValue);
            if (this.participantRequired) {
                attributeValue = XmlParser.valueForActionParticipantAttribute(type, "player", "in_game");
                if (attributeValue != null) {
                    this.inGame = Boolean.valueOf(attributeValue);
                }
            }
        }
    }

    public Event() {

    }

    public boolean dependantOf(String participant) {
        String attributeValue = XmlParser.valueForActionAttribute(type, participant);
        return attributeValue != null && Boolean.valueOf(attributeValue);
    }

    @Override
    public String toString() {
        String res = time + ":00 \t "
                + type + " \t ";
        if (player != null) {

            res += player.participant.value.surName();

            if (this.related != null) {
                res += ";" + this.related.player.participant.value.surName();
            }
        }
        if (result != null) {
            res += " \t score " + result;
        }
        return res;
    }

    public ActionResult toResult() {
        ActionResult res = new ActionResult();
        res.time = time;
        res.action = type;
        res.resume = AppUtils.extractResume(tweets);
        res.tweets = this.tweets;
        if (player != null) {
            res.team = player.participant.value.team.name;
            res.player = player.participant.value.surName();

            if (this.related != null) {
                res.related = this.related.player.participant.value.surName();
            }
        }
        if (result != null) {
            res.score = result;
        }
        return res;
    }

    public boolean hasSameTeamAs(Event event) {
        if (event.isParticipantRequired() && this.isParticipantRequired()) {
            return event.player.participant.value.team.equals(this.player.participant.value.team);
        }
        return !event.isParticipantRequired();
    }

    @Override
    public boolean equals(Object obj
    ) {
        if (obj == null || !(obj instanceof Event)) {
            return false;
        }
        Event other = (Event) obj;
        if (other.type == null || this.type == null || !other.type.equals(this.type)) {
            return false;
        }
        if (this.isParticipantRequired() && other.isParticipantRequired()) {
            return this.player != null
                    && other.player != null
                    && other.player.participant.value.equals(this.player.participant.value);
        }

        return true;
    }

    public Event findSameAsMe(List<Event> myRel) {
        Event otherAction = null;
        if (myRel != null) {
            for (Event event : myRel) {
                if (event.equals(this)) {
                    otherAction = event;
                }
            }
        }
        return otherAction;
    }

    public ActionStatus sameAsOneOf(HashMap<String, ArrayList<Event>> actions) {
        ArrayList<Event> toAdd = toAdd();
        ActionStatus status = XmlParser.isSameAs(actions, this);
        if (status == ActionStatus.EXISTS || status == ActionStatus.NOT_YET) {
            return status;
        }
        if (!actions.containsKey(this.type)) {
            return ActionStatus.NOT_EXIST;
        }
        for (Event event : toAdd) {
            List<Event> myRel = actions.get(event.type);
            Event otherAction = findSameAsMe(myRel);
            if (otherAction == null) {
                status = ActionStatus.NOT_EXIST;
                continue;
            }

            if (!AppUtils.isDiffGreaterThan(event.time, otherAction.time, EventTrackingServer.getInstance().windowInterval * 2)) {
                otherAction.confirmedCount++;
                otherAction.nbTweets = event.nbTweets;
                return ActionStatus.EXISTS;
            }

            if (otherAction.confirmedCount > 1 && event.nbTweets < otherAction.nbTweets) {
                otherAction.confirmedCount++;
                otherAction.nbTweets = event.nbTweets;
                return ActionStatus.EXISTS;
            }

        }
        return status;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 13 * hash + Objects.hashCode(this.type);
        hash = 13 * hash + Objects.hashCode(this.player);
        return hash;
    }

    public boolean merge(Event obj) {

        if (this.equals(obj)) {
            return false;
        }

        if (!obj.getClass().isAssignableFrom(this.getClass())) {
            return false;
        }

        Method[] methods = obj.getClass().getMethods();

        for (Method fromMethod : methods) {
            if (fromMethod.getDeclaringClass().equals(obj.getClass())
                    && (fromMethod.getName().matches("^get[A-Z].*$") || fromMethod.getName().matches("^is[A-Z].*$"))) {

                String fromName = fromMethod.getName();
                String toName;
                if (fromName.matches("^get[A-Z].*")) {
                    toName = fromName.replace("get", "set");
                } else {
                    toName = fromName.replace("is", "set");
                }

                try {
                    Method toMetod = obj.getClass().getMethod(toName, fromMethod.getReturnType());
                    Object value = fromMethod.invoke(this, (Object[]) null);
                    if (value != null) {
                        toMetod.invoke(obj, value);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }

    public void fireNotification(int nb) {
        this.setChanged();
        notifyObservers(nb);
    }

    public ArrayList<Event> toAdd() {
        ArrayList<Event> actions = new ArrayList<>();
        if (this.player != null && this.player.related != null) {
            Event a = new Event(type);
            this.merge(a);
            a.inGame = false;
            this.inGame = true;
            a.player = new ActionParticipant<>();
            a.player.participant = this.player.related;
            this.related = a;
            actions.add(this);
            actions.add(this.related);
        } else {
            actions.add(this);
        }
        return actions;
    }



}
