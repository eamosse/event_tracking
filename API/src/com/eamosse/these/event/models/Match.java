/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.models;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Observable;

/**
 *
 * @author aedouard
 */
public class Match extends Observable{

    public static final ArrayList<Match> TEAMS;

    static {
        TEAMS = new ArrayList<>();
    }

    public Team home, away;
    public double confidance = 1;
    public boolean isConfirmed =false;

    public static void addPlay(Team home, Team away) {
        Match m = new Match(home, away);
       // m.addObserver(Main.matchObserver);
        m.notif();
        
    }

    public Match(Team home, Team away) {
        this.home = home;
        this.away = away;
        //this.notif();
    }
    
    private void notif(){
        this.setChanged();
        this.notifyObservers();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + Objects.hashCode(this.home.name);
        hash = 17 * hash + Objects.hashCode(this.away.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Match other = (Match) obj;
        return other.home.equals(this.home) && other.away.equals(this.away);
    }  

    @Override
    public String toString() {
        return this.home.name + " is playing " + this.away.name + " - " + confidance;
     }
    
}
