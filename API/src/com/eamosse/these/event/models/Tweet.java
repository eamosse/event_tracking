/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.models;

import com.eamosse.these.event.enums.ParticipantType;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author aedouard
 */
@XmlRootElement(name = "tweet")
public class Tweet {

    public String text, created_at, id, tweet_time;
    @XmlTransient
    private final Map<String, Set<Participant>> actions;
    private final ArrayList<Match> matches;
    private Team hold;
    //static Set<Participant> sets;

    public Tweet() {
        this.actions = new HashMap<>();
        this.matches = new ArrayList<>();
    }

    //@XmlJavaTypeAdapter(MapAdapter.class)
    public Map<String, Set<Participant>> getActions() {
        return actions;
    }

    public void addPlay(String home, String away, String homeValue, String awayValue) {

    }

    public void addAction(String action, Set<Participant> participants) {
        if (participants.isEmpty()) {
            Event mAction = new Event(action);
            if (!mAction.participantRequired) {
                participants.add(new Participant(ParticipantType.UNDEFINED, "N/A"));
            }
        }
        participants.add(new Participant(ParticipantType.TIME, toTime()));
        if (actions.containsKey(action)) {
            actions.get(action).addAll(new HashSet<>(participants));
        } else {
            actions.put(action, new HashSet<>(participants));
        }
    }

    public String toTime() {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM d HH:mm:ss zzz yyyy");
            Date d = formatter.parse(created_at);
            SimpleDateFormat format = new SimpleDateFormat("HH:mm");
            return format.format(d);
        } catch (ParseException ex) {
            Logger.getLogger(Tweet.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "00:00";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Tweet)) {
            return false;
        }
        Tweet other = (Tweet) obj;
        return this.id.equals(other.id);
        //return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public String toString() {

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Tweet.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            StringWriter sw = new StringWriter();
            jaxbMarshaller.marshal(this, sw);
            String xmlString = sw.toString();
            return xmlString;
        } catch (JAXBException ex) {
            Logger.getLogger(Tweet.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

}
