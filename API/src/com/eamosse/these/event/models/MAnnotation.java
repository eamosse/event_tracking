/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.models;

import gate.Annotation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

/**
 *
 * @author aedouard
 */
public class MAnnotation implements Comparable<MAnnotation> {

    public Annotation parent;
    public ArrayList<MAnnotation> child = new ArrayList<>();
    boolean hasChild = false;
    public Tweet tweet;
    public String action = "N/A";

    public MAnnotation(Annotation paAnnotation) {
        this.parent = paAnnotation;
    }

    public boolean overlaps(MAnnotation that) {
        return this.parent.getStartNode().getOffset() >= that.parent.getStartNode().getOffset();
    }

    public void sortChildren() {
        Collections.sort(child);
    }

    public MAnnotation findChild(String type) {
        for (MAnnotation ann : child) {
            if (ann.parent.getType().equals(type)) {
                return ann;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        String st = this.parent.toString();
        return st;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj==null || !(obj instanceof MAnnotation)){
            return false;
        }
        MAnnotation other = (MAnnotation)obj;
        
        return this.tweet!=null && (this.tweet.equals(other.tweet));
        //return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.tweet);
        hash = 53 * hash + Objects.hashCode(this.action);
        return hash;
    }

    @Override
    public int compareTo(MAnnotation o) {
        if (this.parent.getStartNode().getOffset() > o.parent.getStartNode().getOffset()) {
            return 1;
        } else if (this.parent.getStartNode().getOffset() < o.parent.getStartNode().getOffset()) {
            return -1;
        } else {
            return 0;
        }
    }
}
