/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.models;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author aedouard
 */
public class Player {

    public String name;
    public ArrayList<String> nicknames;
    public Team team;

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Player)) {
            return false;
        }
        Player p = (Player) obj;
        return p.surName().equals(this.surName());
        //return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.name);
        hash = 59 * hash + Objects.hashCode(this.team);
        return hash;
    }

    public String surName() {
        String[] parts = name.split((" "));
        return parts[parts.length - 1];
    }

}
