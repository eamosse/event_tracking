/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.models;

/**
 *
 * @author aedouard
 */
public interface ActionCallback {
    public void onParticipant(Event action);
    public void onNoParticipant(Event action);
}
