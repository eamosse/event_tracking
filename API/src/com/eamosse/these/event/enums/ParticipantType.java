/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.enums;

/**
 *
 * @author aedouard
 */
public enum ParticipantType {
    TEAM, TIME, PLAYER, RESULT, UNDEFINED, TWEET
}
