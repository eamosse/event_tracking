/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.manager;

import com.eamosse.these.event.models.Team;
import com.eamosse.these.event.mongodb.DBManager;
import com.eamosse.these.event.mongodb.JsonHelper;
import com.google.gson.reflect.TypeToken;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author aedouard
 */
public class TeamManager {

    static HashMap<String, Team> maps;
    static String collectionName = "team";

    static {
        maps = new HashMap<>();
    }

    public static Team findByKey(String key, String[] values) {
        if (maps.containsKey(values[0])) {
            return maps.get(values[0]);
        }
        DBCursor find = DBManager.findByKeyInValues(collectionName, key, values);
        Team team = null;
        Type listType = new TypeToken<ArrayList<String>>() {
        }.getType();
        if (find.hasNext()) {
            team = new Team();
            DBObject next = find.next();
            team.name = next.get("name").toString();
            team.code = next.get("code").toString();
            team.nicknames = JsonHelper.getListObjectFromJson(next.get("nicknames").toString(), listType);
        }
        maps.put(values[0], team);
        return team;
    }
}
