/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.manager;

import com.eamosse.these.event.models.Player;
import com.eamosse.these.event.models.Team;
import com.eamosse.these.event.mongodb.DBManager;
import com.eamosse.these.event.mongodb.JsonHelper;
import com.google.gson.reflect.TypeToken;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author aedouard
 */
public class PlayerManager {

    static HashMap<String, Player> maps;
    static String collectionName = "player";

    static {
        maps = new HashMap<>();
    }

    public static Player findByKey(String key, Object values) {

        String dictKey;
        if (values instanceof String[]) {
            dictKey = ((String[]) values)[0];
        } else {
            dictKey = (String) values;
        }
        if (maps.containsKey(dictKey)) {
            return maps.get(dictKey);
        }
        DBCursor find = DBManager.findByKeyInValues(collectionName, key, values);
        Player player = null;
        Type listType = new TypeToken<ArrayList<String>>() {
        }.getType();
        if (find.hasNext()) {
            player = new Player();
            DBObject next = find.next();
            player.name = next.get("name").toString();
            Team team = TeamManager.findByKey("nicknames", new String[]{next.get("teamName").toString().toLowerCase()});
            player.team = team; //PlayerManager.findByKey("", values)new Team(next.get("teamName").toString());
            player.nicknames = JsonHelper.getListObjectFromJson(next.get("nicknames").toString(), listType);
        }
        maps.put(dictKey, player);
        return player;
    }

}
