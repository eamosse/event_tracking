/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.manager;

import com.eamosse.these.event.models.Tweet;
import com.eamosse.these.event.models.Tweets;
import com.mongodb.BasicDBList;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author aedouard
 */
public class TweetManager {

    public static Tweet fromDBObject(DBObject dbObject) {
        Tweet t = new Tweet();
        t.text = dbObject.get("text").toString().replaceAll("\\s+", " ").toLowerCase();
        t.id = dbObject.get("_id").toString();
        t.created_at = dbObject.get("created_at").toString();
        return t;
    }
    
    public static Tweets fromDBList(DBCursor dbObject) {
        Iterator<DBObject> iterator = dbObject.iterator();
        Tweets tweets = new Tweets(); 
        while(iterator.hasNext()){
            Tweet t = fromDBObject(iterator.next());
            tweets.addTweet(t);
        }
        return tweets; 
    }

}
