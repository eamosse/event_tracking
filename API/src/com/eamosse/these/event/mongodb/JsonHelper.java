package com.eamosse.these.event.mongodb;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import java.util.List;
import java.util.Map;

public class JsonHelper {

    /**
     * Get an instance of a given class from a json object
     *
     * @param <T>
     * @param theJson the json object
     * @param classToReturn the class
     * @param jsonKey
     * @return instance of the given class
     */
    public static <T> T getObjectFromJson(String theJson, Class<T> classToReturn) {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
            @Override
            public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                return new Date(json.getAsJsonPrimitive().getAsLong());
            }
        });
        GsonBuilder gsonb = new GsonBuilder();
        Gson gson = gsonb
                .setDateFormat(DateFormat.LONG)
                .create();

        return gson.fromJson(theJson, classToReturn);
    }

    public static <T> ArrayList<T> getListObjectFromJson(String theJsonObject, Type typeOfTheList) {
        try {
            GsonBuilder gsonb = new GsonBuilder();
            Gson gson = gsonb
                    //.excludeFieldsWithoutExposeAnnotation()
                    .setDateFormat(DateFormat.LONG)
                    .create();
            ArrayList<T> res = gson.fromJson(theJsonObject, typeOfTheList);
            return res;
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return null;
        }
    }

    /**
     *
     * @param toTransformInJson the instance of class to serialize
     * @return a json object from the given instance or null if error occurs
     */
    public static JSONObject createJsonObject(Object toTransformInJson) {
        GsonBuilder gsonb = new GsonBuilder();
        Gson gson = gsonb
                //.excludeFieldsWithoutExposeAnnotation()
                .setDateFormat(DateFormat.LONG)
                .create();

        JSONObject jsonUserModel = new JSONObject();
        try {
            String result = gson.toJson(toTransformInJson);
            jsonUserModel = new JSONObject(result);
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return jsonUserModel;
    }

    /**
     * Create a json array from a list of object
     *
     * @param listOfObjectToTransformInJsonArray the list of objects
     * @return a json array
     */
    @SuppressWarnings("finally")
    public static JSONArray createJsonArray(List<?> listOfObjectToTransformInJsonArray) {
        JSONArray jsonArray = null;
        Gson gson = new GsonBuilder()
                //.excludeFieldsWithoutExposeAnnotation()
                .create();
        try {
            jsonArray = new JSONArray(gson.toJson(listOfObjectToTransformInJsonArray));
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            jsonArray = null;
        } finally {
            return jsonArray;
        }
    }

    public static Map<String, String[]> convertToMap(List<?> list) {
        JSONArray createJsonArray = createJsonArray(list);
        Type mapType = new TypeToken<Map<String, Map>>() {
        }.getType();
        Map<String, String[]> son = new Gson().fromJson(createJsonArray.toString(), mapType);
        return son;
    }

    public static boolean isResponseJson(String result) {
        // TODO Auto-generated method stub
        try {
            new JSONObject(result);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
