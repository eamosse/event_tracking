/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.mongodb;

import com.eamosse.these.event.manager.TeamManager;
import com.eamosse.these.event.manager.TweetManager;
import com.eamosse.these.event.models.Tweets;
import com.mongodb.AggregationOutput;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import java.io.File;
import java.lang.reflect.Type;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bson.types.ObjectId;

/**
 *
 * @author aedouard
 */
public class DBManager {

    public static DB db;

    static {
        connect("hackatal_en");
    }

    public static void connect(String dbName) {
        MongoClient mongoClient;
        try {
            mongoClient = new MongoClient();
            db = mongoClient.getDB(dbName);
        } catch (UnknownHostException ex) {
            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static File findTweetsPerInterval(String collectionName, int hour, int... minute) {
        DBObject groupHour = new BasicDBObject("$hour", "$created_at");
        DBObject groupMinute = new BasicDBObject("$minute", "$created_at");
        DBObject groupID = new BasicDBObject("hour", groupHour);
        groupID.put("minute", groupMinute);
        DBObject groupFields = new BasicDBObject("_id", groupID);
        groupFields.put("count", new BasicDBObject("$sum", 1));
        groupFields.put("data", new BasicDBObject("$addToSet", "$_id"));
        DBObject sortFields = new BasicDBObject("_id.hour", 1);
        sortFields.put("_id.minute", 1);

        //match
        BasicDBList matchCond = new BasicDBList();
        matchCond.add(new BasicDBObject("_id.hour", hour));
        BasicDBObject minutes = new BasicDBObject("$gte", minute[0]);
        minutes.put("$lt", minute[1]);
        matchCond.add(new BasicDBObject("_id.minute", minutes));

        //match 
        BasicDBObject match = new BasicDBObject("$and", matchCond);

        DBCollection myColl = db.getCollection(collectionName);
        AggregationOutput output = myColl.aggregate(new BasicDBObject("$group", groupFields), new BasicDBObject("$match", match), new BasicDBObject("$sort", sortFields));
        DBObject command = output.getCommand();
        Iterator<DBObject> results = output.results().iterator();
        ArrayList<ObjectId> ids = new ArrayList<>();
        while (results.hasNext()) {
            DBObject next = results.next();
            BasicDBList get = (BasicDBList) next.get("data");
            Iterator<Object> iterator = get.iterator();
            while (iterator.hasNext()) {
                ObjectId next1 = (ObjectId) iterator.next();
                ids.add(next1);
            }
        }

        DBCursor cursor = findByKeyInValues(collectionName, "_id", ids.toArray(), "text", "id", "created_at");
        //System.out.println(cursor.getQuery());
        Tweets tweets = TweetManager.fromDBList(cursor);
        if (!tweets.tweet.isEmpty()) {
            File toXml = tweets.toXml("example_" + hour + "_" + minute[0] + "_" + minute[1] + ".xml");
            return toXml;
        }
        return null;

    }

    public static void main(String[] args) {
        find("match", new BasicDBObject(), null);

    }

    public static <T> ArrayList<T> find(String collection, DBObject query, Type typeOfTheList) {
        List<DBObject> find = db.getCollection(collection).find(query).toArray();
        find.stream().map((ob) -> {
            if (ob.containsField("home")){
                ob.put("home", findByKeyInValues("team","nicknames", new String[]{ob.get("home").toString().toLowerCase()}).next());
            }
            if (ob.containsField("away")){
                ob.put("away", findByKeyInValues("team","nicknames", new String[]{ob.get("away").toString().toLowerCase()}).next());
            }
            if (ob.containsField("date")) {
                try {
                    String date = ob.get("date").toString();
                    SimpleDateFormat formatter = new SimpleDateFormat("d MMM yyyy", Locale.US);
                    //Mon Jun 13 02:00:00 CEST 2016
                    ob.put("date", formatter.format(new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy").parse(date)));
                   
                } catch (ParseException ex) {
                    Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            return ob;
        }).forEachOrdered((ob) -> {
            ob.put("id", ob.get("_id").toString());
        });
        return JsonHelper.getListObjectFromJson(find.toString(), typeOfTheList);
    }

    public static DBCursor findByKeyInValues(String collectionName, String key, Object values, String... fields) {

        DBCollection collection = db.getCollection(collectionName);
        DBObject query;
        if (values.getClass().isArray()) {
            query = new BasicDBObject(key, new BasicDBObject("$in", values));
        } else {
            query = new BasicDBObject(key, values);
        }
        DBCursor find;
        if (fields != null && fields.length > 0) {
            BasicDBObject f = new BasicDBObject();
            for (String field : fields) {
                f.append(field, 1);
            }
            find = collection.find(query, f);
        } else {
            find = collection.find(query);
        }

        return find;
    }

}
