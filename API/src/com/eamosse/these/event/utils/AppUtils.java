/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.utils;

import com.eamosse.these.event.models.Tweet;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.MINUTES;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import org.apache.commons.math.stat.StatUtils;
import org.apache.commons.math.util.FastMath;

/**
 *
 * @author aedouard
 */
public class AppUtils {

    public static String toXml(List<Tweet> tweets) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Tweet.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            StringWriter sw = new StringWriter();
            jaxbMarshaller.marshal(tweets, sw);
            String xmlString = sw.toString();
            return xmlString;
        } catch (JAXBException ex) {
            Logger.getLogger(AppUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static Date timeToDate(String stringTime) {
        try {
            DateFormat format = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
            Date date1 = format.parse(stringTime);
            return date1;
        } catch (ParseException ex) {
            Logger.getLogger(AppUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static boolean isWithinAnIntervalOf(Date date1, Date date2, int interval) {
        long MAX_DURATION = MILLISECONDS.convert(interval, MINUTES);
        long val = Math.abs(date2.getTime() - date1.getTime());
        return val >= MAX_DURATION;
    }

    public static boolean isDiffGreaterThan(String time1, String time2, int duration) {

        DateFormat format = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
        Date date1 = timeToDate(time1);
        Date date2 = timeToDate(time2);
        boolean res = isWithinAnIntervalOf(date1, date2, duration);
        return res;
    }

    public static void main(String[] args) {
        System.out.println(isDiffGreaterThan("15:47:14", "16:18:15", 2));
    }

    public static void emptyFolder(String name) {
        File folder = new File(name);
        if (!folder.exists()) {
            folder.mkdir();
        } else {
            for (File f : folder.listFiles()) {
                f.delete();
            }
        }
    }

    public static void deleteIt(String fileName) {
        new File("result").mkdir();
        File f = new File("result/" + fileName);
        if (f.exists()) {
            f.delete();
        }
    }

    public static void logIt(String fileName, String text) {
        try {
            File file = new File("result/" + fileName);
            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
            out.println(text);
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static int[] extractScore(String score) {
        score = score.replace("-", ":");
        int[] st = new int[2];
        int i = -1;
        Scanner s = new Scanner(score);
        s.useDelimiter("\\D+");
        while (s.hasNextInt()) {
            i++;
            if (i < st.length) {
                st[i] = s.nextInt();
            }
        }
        return st;
    }

    public static HashMap<String, Double> computeStat(double[] values, double current) {
        if(values.length > 10){
            values = Arrays.copyOfRange(values, values.length-10, values.length);
        }
        
        double mean = StatUtils.mean(values);
        double std = FastMath.sqrt(StatUtils.variance(values));
        if (std == 0) {
            std = 1;
        }
        double pound = Math.abs(mean - current) / std;

        HashMap<String, Double> map = new HashMap<>();
        map.put("mean", mean == 0 ? current : mean);
        map.put("std", std <= 1 ? mean : std);
        map.put("teta", pound);
        //map.put("current", values[values.length - 1]);
        //map.put("teta", (map.get("current") - map.get("mean")) / map.get("std"));
        return map;

    }

    static int index;

    public static double[] toDouble(List<Double> aList) {
        double[] values = new double[aList.size()];
        index = 0;
        aList.forEach(value -> {
            values[index] = (double) value;
            index++;
        });

        return values;
    }

    public static String extractResume(ArrayList<String> tweets) {
        try {
            final String text = String.join(". ", tweets);
            ProcessBuilder pb = new ProcessBuilder(PropertiesHelper.PYTHON_HOME, PropertiesHelper.PYTHON_PATH + "textrank.py", text);
            Process p = pb.start();
            BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String res = in.readLine();
            return res;
        } catch (IOException ex) {
            Logger.getLogger(AppUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tweets.get(0);
    }

}
