/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.utils;

/**
 *
 * @author aedouard
 */
import com.eamosse.these.event.AnnieHelper;
import com.eamosse.these.event.enums.ActionStatus;
import com.eamosse.these.event.models.Event;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XmlParser {

    public static String valueForActionAttribute(String action, String atribute) {
        Element elementNode = (Element) readFile("//action[@name=\"" + action + "\"]");
        if (elementNode == null) {
            return null;
        }
        return elementNode.getAttribute(atribute);
    }

    public static String valueForActionParticipantAttribute(String action, String participant, String atribute) {
        Element elementNode = (Element) readFile("//action[@name=\"" + action + "\"]/" + participant);
        if (elementNode == null) {
            return null;
        }
        return elementNode.getAttribute(atribute);
    }

    public static ActionStatus isSameAs(HashMap<String, ArrayList<Event>> actions, Event action) {
        ActionStatus status = ActionStatus.NOT_EXIST;

        Element intervalNode = (Element) readFile("//action[@name=\"" + action.type + "\"]/interval");
        if (intervalNode != null) {
            String intAction = intervalNode.getAttribute("action");
            int maxDuration = Integer.valueOf(intervalNode.getAttribute("duration"));
            //the action must exist in the list
            ArrayList<Event> depActions = actions.get(intAction);
            if (depActions == null) { //the action of which it depends does not exist 
                status = ActionStatus.NOT_YET;
                return status;
            } else {
                //get the latest action 
                Event latest = depActions.get(depActions.size() - 1);
                if (!AppUtils.isDiffGreaterThan(latest.time, action.time, maxDuration)) {
                    status = ActionStatus.NOT_YET;
                    return status;
                }
            }
        }

        Element elementNode = (Element) readFile("//action[@name=\"" + action.type + "\"]");
        if (!actions.containsKey(action.type)) {
            return ActionStatus.NOT_EXIST;
        }
        if (elementNode != null) {
            ArrayList<Event> pActions = actions.get(action.type);
            boolean isPlayerDependant = Boolean.valueOf(elementNode.getAttribute("isPlayerDependant"));
            boolean isTeamDependant = Boolean.valueOf(elementNode.getAttribute("isTeamDependant"));
            if (!isPlayerDependant && !isTeamDependant) {
                int maxOccurence = Integer.valueOf(elementNode.getAttribute("max_occcurence"));
                status = pActions.size() <= maxOccurence ? ActionStatus.EXISTS : ActionStatus.NOT_EXIST;
            }
            if (isPlayerDependant) {
                //get the player node
                Element playerNode = (Element) readFile("//action[@name=\"" + action.type + "\"]/player");
                int maxOccurence = Integer.valueOf(playerNode.getAttribute("max_occcurence"));
                status = pActions.stream().filter(mAction -> mAction.type.equals(action.type) && mAction.player != null && mAction.player.equals(action.player)).count() < maxOccurence ? ActionStatus.NOT_EXIST : ActionStatus.EXISTS;
            }

            /*
            if (isTeamDependant) {
                //get the player node
                Element teamNode = (Element) readFile("//action[@name=\"" + action.type + "\"]/team");
                int maxOccurence = Integer.valueOf(teamNode.getAttribute("max_occcurence"));
                return pActions.stream().filter(mAction -> mAction.type.equals(action.type) && mAction.team.equals(action.team)).count() < maxOccurence ? ActionStatus.NOT_EXIST : ActionStatus.EXISTS;
             */
        }
        return status;
    }

    private static Node readFile(String query) {
        /*
         * Etape 1 : récupération d'une instance de la classe "DocumentBuilderFactory"
         */
        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        try {
            /*
             * Etape 2 : création d'un parseur
             */
            final DocumentBuilder builder = factory.newDocumentBuilder();

            /*
	     * Etape 3 : création d'un Document
             */
            final Document document = builder.parse(new File(PropertiesHelper.RULES_PATH+"actions.xml"));

            /*
	     * Etape 4 : récupération de l'Element racine
             */
            final Element racine = document.getDocumentElement();

            /*
	     * Etape 5 : récupération des personnes
             */
            final NodeList racineNoeuds = racine.getChildNodes();
            final int nbRacineNoeuds = racineNoeuds.getLength();

            XPathFactory xPathfactory = XPathFactory.newInstance();
            XPath xpath = xPathfactory.newXPath();
            XPathExpression expr = xpath.compile(query);
            NodeList nl = (NodeList) expr.evaluate(document, XPathConstants.NODESET);
            if (nl.getLength() > 0) {
                return nl.item(0);
            }
        } catch (final ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        } catch (XPathExpressionException ex) {
            Logger.getLogger(XmlParser.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static void main(final String[] args) throws XPathExpressionException {
        /*MAction action = new MAction(); 
        action.type = "CRO";
        System.out.println(isSameAs(null, action));
         */

        String readFile = valueForActionParticipantAttribute("CJA", "player", "in_game");
        System.out.println(readFile);
    }
}
