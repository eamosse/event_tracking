/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aedouard
 */
public class PropertiesHelper {

    public static String GATE_HOME;
    public static String JAPE_PATH;
    public static String PYTHON_PATH;
    public static String RULES_PATH;
    public static String PYTHON_HOME;

    public static void loadKey() {
        try {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            InputStream input = classLoader.getResourceAsStream("parameters.properties");
            Properties properties = new Properties();
            properties.load(input);
            GATE_HOME = (String) properties.get("GATE_HOME");
            JAPE_PATH = (String) properties.get("JAPE_PATH");
            RULES_PATH = (String) properties.get("RULES_PATH");
            PYTHON_PATH = (String) properties.get("PYTHON_PATH");
            PYTHON_HOME = (String) properties.get("PYTHON_HOME");
        } catch (IOException ex) {
            Logger.getLogger(PropertiesHelper.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
