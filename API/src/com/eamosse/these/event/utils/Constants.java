/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.utils;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author aedouard
 */
public class Constants {

    public static final Map<String, Double> tresholds;
    public static final Map<String, Double> actionPounds;
    public static final double confidence = 0.20;
    public static final int minNbTweets = 100;

    static {
        tresholds = new HashMap<>();
        tresholds.put("team", 0.4);
        tresholds.put("player", 0.4);
        tresholds.put("result", 0.25);
        tresholds.put("time", 0.80);
        tresholds.put("tweet_time", 0.3);

        actionPounds = new HashMap<>();
        actionPounds.put("BUT", 5.0);
        actionPounds.put("CJA", 1.5);
        actionPounds.put("CRO", 2.5);
        actionPounds.put("PEN", 2.5);
        //actionPounds.put("OFF", 0.05);
        actionPounds.put("CGT", 1.5);
        actionPounds.put("TIR", 0.5);
        actionPounds.put("D1P", 0.05);
        actionPounds.put("D2P", 0.05);
        actionPounds.put("F1P", 0.05);
        actionPounds.put("F2P", 0.05);

        /*actionRatio = new HashMap<>();
         actionPounds.put("score", );
         actionPounds.put("card", 0.5);
         actionPounds.put("penalty", 0.8);
         actionPounds.put("offside", 0.2);
         actionPounds.put("substitution", 0.2);
         */
    }
}
