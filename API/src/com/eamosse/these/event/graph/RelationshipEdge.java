/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.graph;

import com.eamosse.these.event.models.Participant;
import java.util.ArrayList;
import org.jgrapht.graph.DefaultEdge;

/**
 *
 * @author aedouard
 * @param <V>
 */
public class RelationshipEdge<V> extends DefaultEdge {

    public final V action;
    public final Participant participant;
    public final String label;
    public final Participant related;
    public int nb = 1;
    public ArrayList<String> tweets = new ArrayList<>();

    public RelationshipEdge(V action, Participant participant, String label, Participant related, String  tweet) {
        this.action = action;
        this.participant = participant;
        this.label = label;
        this.related = related;
        this.tweets.add(tweet);
    }

    public V getAction() {
        return action;
    }

    public Participant getParticipant() {
        return participant;
    }

    @Override
    public String toString() {
        return getLabel();
    }
    
   

    /**
     * @return the label
     */
    public String getLabel() {
        //return action + " " + label + " " + participant + " " + nb;
        return label;// + ":" + participant + " " + nb;
    }

}
