/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.graph;

import com.eamosse.these.event.models.Action;
import com.eamosse.these.event.models.Event;
import com.eamosse.these.event.models.Participant;
import com.eamosse.these.event.enums.ParticipantType;
import com.eamosse.these.event.server.EventTrackingServer;
import com.eamosse.these.event.utils.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.jgrapht.graph.ClassBasedEdgeFactory;
import org.jgrapht.graph.DirectedMultigraph;

/**
 *
 * @author aedouard
 */
public class JGraphManager {

    HashMap<String, String> maps = new HashMap<>();
    DirectedMultigraph<String, RelationshipEdge> graph
            = new DirectedMultigraph<>(
                    new ClassBasedEdgeFactory<>(RelationshipEdge.class));
    public int nbTweets = 0;
    public int actionTweets = 0;

    public ArrayList<Action> footActions = new ArrayList<>();
    int toInc = 1;

    public void deInit() {
        nbTweets = 0;
        actionTweets = 0;
        footActions = null;
    }

    public void addToGraph(String tweet, Map<String, Set<Participant>> map) {

        actionTweets++;

        for (String action : map.keySet()) {
            //System.out.println("ADDING... "+action);
            toInc = 1;
            Set<Participant> participants = map.get(action);
            //System.out.println(participants);

            if (!graph.containsVertex(action)) {

                graph.addVertex(action);
            }

            if (participants.isEmpty()) {
                Event mAction = new Event(action);
                if (!mAction.participantRequired) {
                    participants.add(new Participant(ParticipantType.UNDEFINED, "N/A"));
                }
            }

            participants.stream().map((participant) -> {
                if (!graph.containsVertex(participant.value.toString())) {
                    graph.addVertex(participant.value.toString());
                }
                return participant;
            }).forEach((participant) -> {
                if (graph.containsEdge(action, participant.value.toString())) {
                    increase(action, participant, tweet);
                } else {
                    if (participant.related != null) {
                        graph.addEdge(action, participant.value.toString(), new RelationshipEdge(action, participant, participant.type.name(), participant.related, tweet));
                    } else {
                        graph.addEdge(action, participant.value.toString(), new RelationshipEdge(action, participant, participant.type.name(), null, tweet));

                    }
                }
            });

        }
    }

    void increase(String action, Participant participant, String tweet) {
        Set<RelationshipEdge> allEdges = graph.getAllEdges(action, participant.value.toString());
        allEdges.stream().forEach((edge) -> {
            edge.nb++;
            edge.tweets.add(tweet);
        });
    }

    class MObject {

        public String action, team, player, time;
    }

    public Stream<RelationshipEdge> search(String action, Participant participant, String type) {
        if (graph.containsVertex(action)) {
            Set<RelationshipEdge> edges = graph.edgesOf(action);

            switch (type) {
                case "CGT_OUT":
                    return edges.stream().filter(edge -> edge.participant.related != null && edge.participant.related.equals(participant) && edge.nb > 3);
                default:
                    return edges.stream().filter(edge -> edge.participant.equals(participant) && edge.nb > 3);
            }
        }
        return null;

    }

    void prune() {
        //String target[] = new String["CGT-IN", "CGT_];
        Map<String, List<RelationshipEdge>> collect = graph.edgeSet().stream().collect(Collectors.groupingBy(w -> w.getAction().toString()));
        collect.keySet().stream().forEach(action -> {
            if (action.equals("CGT_IN") || action.equals("CGT_OUT")) {
                List<RelationshipEdge> participants = collect.get(action);
                participants.stream().filter((participant) -> participant.label.equals(ParticipantType.PLAYER.name())).forEach((edge) -> {

                    Stream<RelationshipEdge> search = search("CGT", edge.participant, action);
                    if (search != null) {
                        search.forEach((_edge) -> {
                            _edge.nb += edge.nb;
                        });
                        graph.removeEdge(edge);
                    }
                });
            }
        });
    }
    RelationshipEdge maxScore = null, maxTeam = null, maxTime = null;

    public void compute() {
        prune();
        //System.out.println("Tweets " + nbTweets + " Action tweets " + actionTweets);
        Map<String, List<RelationshipEdge>> collect = graph.edgeSet().stream().collect(Collectors.groupingBy(w -> w.getAction().toString()));
        collect.keySet().stream().forEach(action -> {
            if (Constants.actionPounds.containsKey(action)) {
                // System.out.println("===" + action + "===");
                //get the relation for this action
                List<RelationshipEdge> participants = collect.getOrDefault(action, new ArrayList<>());
                if (participants.isEmpty()) {
                    return;
                }
                maxScore = null;
                maxTeam = null;
                maxTime = null;

                Collections.sort(participants, new mComparator());
                Optional<RelationshipEdge> filter = participants.stream().filter((participant) -> participant.label.equals(ParticipantType.TIME.name())).findFirst();
                if (filter.isPresent()) {
                    maxTime = filter.get();
                } else {
                }
                filter = participants.stream().filter((participant) -> participant.label.equals(ParticipantType.RESULT.name())).findFirst();

                if (filter.isPresent()) {
                    maxScore = filter.get();
                }
                filter = participants.stream().filter((participant) -> participant.label.equals(ParticipantType.TEAM.name())).findFirst();
                if (filter.isPresent()) {
                    maxTeam = filter.get();
                }

                Stream<RelationshipEdge> relations = participants.stream().filter((participant) -> participant.label.equals(ParticipantType.PLAYER.name()) || participant.label.equals(ParticipantType.UNDEFINED.name())); //.findFirst();
                relations.forEach(edge -> {
                    Action footAction = new Action(edge.nb);
                    footAction.addObserver(EventTrackingServer.getInstance().observer);
                    footAction.addRelation(maxTime);
                    footAction.addRelation(maxScore);
                    footAction.addRelation(maxTeam);
                    footAction.addRelation(edge);
                    footActions.add(footAction);

                });
            }

        });

    }

    public static class mComparator implements Comparator<RelationshipEdge> {

        @Override
        public int compare(RelationshipEdge o1, RelationshipEdge o2) {
            if (o1.nb > o2.nb) {
                return -11;
            }
            if (o1.nb == o2.nb) {
                return 0;
            } else {
                return 1;
            }
        }

    }

}
