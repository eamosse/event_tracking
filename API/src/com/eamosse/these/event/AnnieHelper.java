/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event;

import com.eamosse.these.event.utils.PropertiesHelper;
import gate.Corpus;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.Gate;
import gate.ProcessingResource;
import gate.creole.ResourceInstantiationException;
import gate.creole.SerialAnalyserController;
import gate.util.GateException;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aedouard
 */
public class AnnieHelper {

    //public static String gate_home = "/user/aedouard/home/GATE/";
    //public static String japeFolder = "/user/aedouard/home/Documents/_dev/event_tracking/ressources/jape";

    private SerialAnalyserController annieController;
    public Corpus corpus;

    public void createCorpus() {
        try {
            corpus = Factory.newCorpus("StandAloneAnnie corpus");
            this.setCorpus(corpus);
        } catch (ResourceInstantiationException ex) {
            Logger.getLogger(AnnieHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addDocument(File f) throws MalformedURLException, ResourceInstantiationException {
        try {
            // File f = new File(fileName);
            URL u = f.toURI().toURL();
            FeatureMap params = Factory.newFeatureMap();
            params.put("sourceUrl", u);
            params.put("encoding", "utf-8");
            params.put("preserveOriginalContent", true);
            params.put("collectRepositioningInfo", false);
            params.put("markupAware", true);
            Document document = (Document) Factory.createResource("gate.corpora.DocumentImpl", params);
            corpus.add(document);
        } catch (MalformedURLException | ResourceInstantiationException e) {
        }

    }

    public void initAnnie() throws GateException, MalformedURLException {
        String filePath = PropertiesHelper.GATE_HOME + "/plugins/ANNIE/resources/gazetteer/";

        Gate.getCreoleRegister().registerDirectories(
                new File(Gate.getPluginsHome(), "ANNIE").toURL()
        );
        Gate.getCreoleRegister().registerDirectories(
                new File(Gate.getPluginsHome(), "Tools").toURL()
        );
        Gate.getCreoleRegister().registerDirectories(
                new File(Gate.getPluginsHome(), "Twitter").toURL()
        );
        /*Gate.getCreoleRegister().registerDirectories(
                new File(Gate.getPluginsHome(), "Stemmer_Snowball").toURL()
        );*/

        // Gate.getCreoleRegister().registerDirectories(new URL("jar:file:/Applications/GATE_Developer_8.2/plugins/ANNIE/"));
        /**
         * The Corpus Pipeline application to contain ANNIE
         */
        // create a serial analyser controller to run ANNIE with
        annieController = (SerialAnalyserController) Factory.createResource("gate.creole.SerialAnalyserController",
                Factory.newFeatureMap(),
                Factory.newFeatureMap(),
                "ANNIE_" + Gate.genSym()
        );

        // Load normalizer
        ProcessingResource normalizer = (ProcessingResource) Factory.createResource("gate.twitter.Normaliser",
                Factory.newFeatureMap());
        annieController.add(normalizer);

        // Load sentence splitter
        /*ProcessingResource stemmer = (ProcessingResource) Factory.createResource("stemmer.SnowballStemmer",
                Factory.newFeatureMap());

        annieController.add(stemmer);
         */
        // Load tokenizer
        ProcessingResource tokeniser = (ProcessingResource) Factory.createResource("gate.twitter.tokenizer.TokenizerEN",
                Factory.newFeatureMap());
        annieController.add(tokeniser);
        // Load sentence splitter
        ProcessingResource split = (ProcessingResource) Factory.createResource("gate.creole.splitter.SentenceSplitter",
                Factory.newFeatureMap());

        annieController.add(split); 

        // Load POS tagger
        ProcessingResource postagger = (ProcessingResource) Factory.createResource("gate.creole.POSTagger",
                Factory.newFeatureMap());
        annieController.add(postagger);

        // Load Gazetteer -- this is a two step process
        FeatureMap gazetteerFeatures = Factory.newFeatureMap();
        gazetteerFeatures.put("encoding", "UTF-8");

        // Step one: Locate the gazetteer file
        try {
            URL gazetteerURL
                    = new File(filePath + "lists.def").toURL();
            gazetteerFeatures.put("listsURL", gazetteerURL);
            gazetteerFeatures.put("caseSensitive", false);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        // Step two: Load the gazetteer from the file
        ProcessingResource gazetteer = (ProcessingResource) Factory.createResource("gate.creole.gazetteer.DefaultGazetteer",
                gazetteerFeatures);
        gazetteer.setParameterValue("caseSensitive", false);

        annieController.add(gazetteer);

        addGrammar();

        // Load Ortho Matcher
        /*ProcessingResource orthoMatcher = (ProcessingResource) Factory.createResource("gate.creole.orthomatcher.OrthoMatcher",
                Factory.newFeatureMap());

        annieController.add(orthoMatcher);
         */
        //this.createCorpus();
    } // initAnnie()

    void addGrammar() throws ResourceInstantiationException {
        File grammarFOlder = new File(PropertiesHelper.JAPE_PATH);
        for (File file : grammarFOlder.listFiles()) {
            if (file.isHidden()) {
                continue;
            }
            // Load Grammar -- similar to gazetteer
            FeatureMap grammarFeatures = Factory.newFeatureMap();

            try {
                URL grammarURL
                        = file.toURL();
                grammarFeatures.put("grammarURL", grammarURL);
            } catch (MalformedURLException e) {
            }

            ProcessingResource grammar = (ProcessingResource) Factory.createResource("gate.creole.ANNIETransducer",
                    grammarFeatures);

            annieController.add(grammar);
        }

    }

    /**
     * Tell ANNIE's controller about the corpus you want to run on
     *
     * @param corpus
     */
    public void setCorpus(Corpus corpus) {
        annieController.setCorpus(corpus);
    } // setCorpus

    public Corpus getCorpus() {
        return corpus;
    }

    /**
     * Run ANNIE
     *
     * @param files
     * @throws gate.util.GateException
     * @throws java.net.MalformedURLException
     */
    public void execute(ArrayList<File> files) throws GateException, MalformedURLException {
        this.createCorpus();
        for (File file : files) {
            this.addDocument(file);
        }
        annieController.execute();

        //cleanup();
        //Out.prln("...ANNIE complete");
    } // execute()

    public void cleanup() {
        if (!corpus.isEmpty()) {
            for (int i = 0; i < corpus.size(); i++) {
                Document doc1 = (Document) corpus.remove(i);
                corpus.unloadDocument(doc1);
                Factory.deleteResource(doc1);
            }
            Factory.deleteResource(corpus);
        }

    }
}
