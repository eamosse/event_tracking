/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event;

import com.eamosse.these.event.enums.ParticipantType;
import com.eamosse.these.event.models.Action;
import com.eamosse.these.event.models.ActionCallback;
import com.eamosse.these.event.models.ActionParticipant;
import com.eamosse.these.event.models.Event;
import com.eamosse.these.event.models.Participant;
import com.eamosse.these.event.server.EventTrackingServer;
import com.eamosse.these.event.utils.AppUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author aedouard
 */
public class EventAnalyzer {

    static HashMap<String, Double> thresholds = new HashMap<>();
    public static ArrayList<Integer> tweets = new ArrayList<>();
    public static HashMap<ActionParticipant, List<Event>> actionParticipants = new HashMap<>();
    static ArrayList<ArrayList<Event>> observedActions = new ArrayList<>();

    public static void analyze(ArrayList<Action> actions, int nb) {
        System.out.println(actions);
        if (!tweets.isEmpty()) {
            computeThresholdAction(actions, nb);
        }
    }

    public static void computeThresholdAction(ArrayList<Action> actions, int nb) {
        Map<String, List<Action>> actionsGroup
                = actions.stream().collect(Collectors.groupingBy(w -> w.type));
        List<Double> map = tweets.stream().map((Integer t) -> (double) t).collect(Collectors.toList());
        HashMap<String, Double> scores = AppUtils.computeStat(AppUtils.toDouble(map), nb);
        actionsGroup.keySet().forEach((type) -> {
            double empirical_threshold = StreamAnalyzer.getInstance().game.thresholedForAction(type);
            System.out.println(type + " ==> " + Math.abs(empirical_threshold * scores.get("teta")));
            List<Action> actionOfAType = actionsGroup.get(type);
            ArrayList<Event> events = new ArrayList<>();
            actionOfAType.stream().forEach((action) -> {
                eventFromAction(null, action, new ActionCallback() {
                    @Override
                    public void onParticipant(Event event) {
                        //Action with participants 
                        if (actionParticipants.containsKey(event.player)) {
                            List<Double> map = actionParticipants.get(event.player).stream().map(a -> {
                                return (double) a.nbActionTweets;
                            }).collect(Collectors.toList());
                            HashMap<String, Double> statPlayer = AppUtils.computeStat(AppUtils.toDouble(map), event.nbTweets);
                            System.out.println(event.player + " " + statPlayer.get("teta"));
                            event.pound = statPlayer.get("teta");
                            events.add(event);
                                    
                        } else {
                            actionParticipants.put(event.player, new ArrayList<>());
                        }
                        actionParticipants.get(event.player).add(event);
                    }

                    @Override
                    public void onNoParticipant(Event event) {
                        //Action without participant
                    }
                });
            });
            double count = events.stream().mapToDouble(event -> {
                return event.pound;
            }).sum();
            events.stream().forEach((event) -> {
                event.isValid = event.pound / count >= empirical_threshold;
                if (event.isValid) {
                    System.out.println(event);
                }
            });
        });
    }

    static void eventFromAction(Event current, Action a, ActionCallback callback) {
        if (current == null) {
            current = new Event(a.type);
            current.addObserver(EventTrackingServer.getInstance().actionObserver);
        }

        current.nbActionTweets = a.actionTweets;

        if (current.isParticipantRequired()) {
            ActionParticipant player = a.getParticipantWithType(ParticipantType.PLAYER);
            ActionParticipant team = a.getParticipantWithType(ParticipantType.TEAM);
            if (player != null) {
                current.player = player;
                current.nbTweets = player.confidence;
                current.tweets.addAll(player.tweets);
            } else {
                //callback.onNoParticipant(current);
                return;
            }
        }
        //let try to get the time and evantually the score 
        ActionParticipant time = a.getParticipantWithType(ParticipantType.TIME);
        if (time != null) {
            current.time = time.participant.value.toString();
            if (!current.isParticipantRequired()) {
                current.nbTweets = time.confidence;
                current.tweets.addAll(time.tweets);
            }
        }
        ActionParticipant result = a.getParticipantWithType(ParticipantType.RESULT);

        current.frequency = current.nbTweets;
        //parse(current);
        if (current.isParticipantRequired()) {
            callback.onParticipant(current);
        } else {
            callback.onNoParticipant(current);
        }
        // return current;
    }
    static int index = 0;
    static int val = 0;

    static void parse(Event current) {

        if (observedActions.isEmpty()) {
            current.pound = current.nbActionTweets;
            return;
        }
        double values[] = new double[observedActions.size() + 1];
        observedActions.stream().forEach((actions) -> {
            val = 0;
            actions.stream().filter((anAction) -> anAction.equals(current)).forEach((anAction) -> {
                val += anAction.nbActionTweets;
            });
            values[index] = (double) val;
            index++;
        });
        values[index] = current.nbActionTweets;

        HashMap<String, Double> stats = AppUtils.computeStat(values, current.nbTweets);
        double mean = stats.get("mean");
        double std = stats.get("std");
        double pound = stats.get("pound");

        current.pound = stats.get("pound");
    }

}
