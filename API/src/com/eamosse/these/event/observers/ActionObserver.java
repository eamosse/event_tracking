/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.observers;

import com.eamosse.these.event.StreamAnalyzer;
import com.eamosse.these.event.models.Action;
import com.eamosse.these.event.models.ActionParticipant;
import com.eamosse.these.event.models.Event;
import com.eamosse.these.event.enums.ParticipantType;
import com.eamosse.these.event.models.ActionCallback;
import com.eamosse.these.event.models.Player;
import com.eamosse.these.event.models.Team;
import com.eamosse.these.event.server.EventTrackingServer;
import com.eamosse.these.event.utils.AppUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author aedouard
 */
public class ActionObserver implements Observer {

    HashMap<String, ArrayList<Event>> confirmedActions;
    //MAction observedAction;
    ArrayList<Event> holdObservedAction;
    int home = 0, away = 0;
    int index = 0;
    ArrayList<Event> currentActions;// = new ArrayList<>();
    ArrayList<ArrayList<Event>> observedActions = new ArrayList<>();
    ArrayList<Action> tempActions;

    public void deInit() {
        holdObservedAction = null;
        tempActions = null;
        observedActions = null;
        currentActions = null;
    }

    public ActionObserver() {
        this.confirmedActions = new HashMap<>();
        holdObservedAction = new ArrayList<>();
    }

    ActionParticipant highest(Action a, ParticipantType type) {
        int nb = 0;
        ActionParticipant highest = null;
        for (ActionParticipant ap : a.participants) {
            if (ap.participant == null ? type.name().toLowerCase() == null : ap.participant.type.equals(type)) {
                if (nb < ap.confidence) {
                    nb = ap.confidence;
                    highest = ap;
                }
            }
        }
        return highest;
    }

    boolean isValid(Action a, double pound) {
        boolean isConfirmed = false;

        ActionParticipant player = a.getParticipantWithType(ParticipantType.PLAYER);
        if (player != null) {
            Player _player = (Player) player.participant.value;
            computeConfirmation(player, _player.team, pound);
            isConfirmed = player.isConfirmed;
        }
        return isConfirmed;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof Action) {

            Action a = (Action) o;
            tempActions.add(a);
            /*
            Event current = null;
            mActionFromAction(current, a, new ActionCallback() {
                @Override
                public void onParticipant(Event current) {
                    
                    
                    boolean isConfirmed;
                    isConfirmed = isValid(a, current.nbActionTweets);
                    /*if (!isConfirmed) {
                        isConfirmed = merge(a, current);
                    }*/
 /*doConfirm(a, current, isConfirmed);
                }

                @Override
                public void onNoParticipant(Event current) {
                    ActionParticipant p1 = a.getParticipantWithType(ParticipantType.UNDEFINED);
                    boolean isConfirmed; //= merge(a, current);
                    if (p1 != null) {
                        computeConfirmation(p1, null, current.nbActionTweets);
                        isConfirmed = p1.isConfirmed;
                        /*if (!isConfirmed) {
                            isConfirmed = merge(a, current);
                        }*/
 /*doConfirm(a, current, isConfirmed);

                    }
                }
            });*/

        }
    }

    void doConfirm(Action a, Event current, boolean isConfirmed) {
        if (current.nbActionTweets < 2) {
            return;
        }
        if (isConfirmed) {
            /*String past = null;
            if ((current.type.equals("PEN") || current.type.equals("TIR")) && current.result != null) {
                current.type = "BUT";
                past = current.type;
            }

            if (current.type.equals("BUT")) {
                if (current.result != null) {
                    int[] scores = AppUtils.extractScore(current.result);
                    String cc = scores[0] + "" + scores[1];
                    String ha = (home + 1) + "" + away;
                    String ah = home + "" + (away + 1);
                    if (cc.equals(ha) || cc.equals(ah)) {
                        home = scores[0];
                        away = scores[1];
                    } else {
                        //current.type = past;
                        return;
                    }
                }
            }*/
            //current.tweets = a.tweets;
            current.fireNotification(a.actionTweets);
        } else {
            int _index = -1;
            for (Event passedAction : holdObservedAction) {

                _index++;
                if (current.isParticipantRequired() && passedAction.equals(current) && !AppUtils.isDiffGreaterThan(passedAction.time, current.time, EventTrackingServer.getInstance().windowInterval)) {
                    ActionParticipant player = passedAction.action.getParticipantWithType(ParticipantType.PLAYER);
                    player.confidence += current.nbTweets;
                    if (passedAction.nbTweets < current.nbTweets || isValid(passedAction.action, passedAction.nbTweets)) {
                        if (current.result != null && passedAction.result == null) {
                            passedAction.result = current.result;

                        }
                        passedAction.tweets.addAll(passedAction.tweets);
                        passedAction.fireNotification(a.actionTweets);
                        holdObservedAction.remove(_index);
                        return;
                    }

                }
                //holdObservedAction.add(current);
            }

            current.action = a;
            holdObservedAction.add(current);
        }
    }

    public void newBatch() {
        if (currentActions != null) // observedActions.add(new ArrayList<>());
        {
            observedActions.add(new ArrayList<>(currentActions));
        }
        tempActions = new ArrayList<>();
        currentActions = new ArrayList<>();
    }

    int val = 0;

    void mActionFromAction(Event current, Action a, ActionCallback callback) {
        if (current == null) {
            current = new Event(a.type);
            current.addObserver(EventTrackingServer.getInstance().actionObserver);
        }

        //currentActions.add(current);
        current.nbActionTweets = a.actionTweets;

        if (current.isParticipantRequired()) {
            ActionParticipant player = a.getParticipantWithType(ParticipantType.PLAYER);
            ActionParticipant team = a.getParticipantWithType(ParticipantType.TEAM);
            if (player != null) {
                current.player = player;
                current.nbTweets = player.confidence;
                current.tweets.addAll(player.tweets);
            } else {
                //callback.onNoParticipant(current);
                return;
            }
        }
        //let try to get the time and evantually the score 
        ActionParticipant time = a.getParticipantWithType(ParticipantType.TIME);
        if (time != null) {
            current.time = time.participant.value.toString();
            if (!current.isParticipantRequired()) {
                current.nbTweets = time.confidence;
                current.tweets.addAll(time.tweets);
            }
        }
        ActionParticipant result = a.getParticipantWithType(ParticipantType.RESULT);
        /*if (result != null) {
            current.result = result.participant.value.toString();
            current.tweets.addAll(result.tweets);
        } else {
            if (current.type.equals("BUT") && !EventTrackingServer.getInstance().actionObserver.isSameAsAnExist(current)) {
                current.type = "TIR";
            }
        }*/
        current.frequency = current.nbTweets;
        parse(current);
        if (current.isParticipantRequired()) {
            callback.onParticipant(current);
        } else {
            callback.onNoParticipant(current);
        }
        // return current;
    }

    void parse(Event current) {

        if (observedActions.isEmpty()) {
            current.pound = current.nbActionTweets;
            return;
        }
        double values[] = new double[observedActions.size() + 1];
        index = 0;
        observedActions.stream().forEach((actions) -> {
            val = 0;
            actions.stream().filter((anAction) -> anAction.equals(current)).forEach((anAction) -> {
                val += anAction.nbActionTweets;
            });
            values[index] = (double) val;
            index++;
        });
        values[index] = current.nbActionTweets;

        HashMap<String, Double> stats = AppUtils.computeStat(values, current.nbTweets);
        double mean = stats.get("mean");
        double std = stats.get("std");
        double pound = stats.get("pound");

        current.pound = stats.get("pound");
    }

    public void computeConfirmation(ActionParticipant participant, Team team, double nbActions) {
        double teamRatio = team != null ? EventTrackingServer.getInstance().stats.getOrDefault(team, EventTrackingServer.getInstance().gameRatio) : EventTrackingServer.getInstance().gameRatio;
        double pound = StreamAnalyzer.getInstance().game.thresholedForAction(participant.action);//participant.action, 1350.0);
        System.out.println(participant.action + " " + pound + " " + teamRatio + " " + nbActions);
        participant.isConfirmed = Math.abs(participant.confidence * (nbActions / teamRatio)) >= pound && participant.confidence > 2;
    }
    
}
