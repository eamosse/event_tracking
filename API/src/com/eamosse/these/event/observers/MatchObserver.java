/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.observers;

import com.eamosse.these.event.models.Match;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author aedouard
 */
public class MatchObserver implements Observer {

    public final ArrayList<Match> TEAMS;
    private int nbTweets=0;
    private final double treshold = 0.01;

    public void setNbTweets(int nbTweets) {
        this.nbTweets+= nbTweets;
    }
    
    public MatchObserver() {
        this.TEAMS = new ArrayList<>();
    }
    
    @Override
    public void update(Observable o, Object arg) {
        if( o==null || !(o instanceof Match) ) return;
        Match m = (Match)o;
        if (!TEAMS.contains(m)) {
            TEAMS.add(m);
        } else {
            TEAMS.get(TEAMS.indexOf(m)).confidance++;
        }
        
       TEAMS.stream().forEach(match -> {
           //System.out.println("========>"+match.confidance );
           if(!match.isConfirmed && match.confidance/nbTweets >= treshold){
               match.isConfirmed = true;
           }
       });
        
        
    }

}
