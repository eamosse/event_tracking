/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eamosse.these.event.observers;

import com.eamosse.these.event.enums.ActionStatus;
import com.eamosse.these.event.models.Event;
import com.eamosse.these.event.server.EventTrackingServer;
import com.eamosse.these.event.utils.AppUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author aedouard
 */
public class EventObserver implements Observer {

    public HashMap<String, ArrayList<Event>> confirmedActions;

    public ArrayList<String> out;

    public EventObserver() {
        this.confirmedActions = new HashMap<>();
        out = new ArrayList<>();
    }

    public void deInit() {
        out = null;
        confirmedActions = null;
    }

    private void addAction(Event e) {
        if (this.confirmedActions.containsKey(e.type)) {
            this.confirmedActions.get(e.type).add(e);
        } else {
            ArrayList<Event> actions = new ArrayList<>();
            actions.add(e);
            this.confirmedActions.put(e.type, actions);
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof Event) {
            Event action = (Event) o;
            processAction(action, arg);
            /*switch (EventTrackingServer.getInstance().gameStatus) {
                case 0: //game on pause 
                    if (action.allowedInPause) {
                        
                    }
                    processAction(action, arg);
                    break;

                case 1: //game palying 
                    processAction(action, arg);
                    break;
            }*/

        }
    }

    public boolean isSameAsAnExist(Event action) {

        Event otherAction = null;
        ArrayList<Event> myRel = confirmedActions.get(action.type);
        if (myRel != null) {
            for (Event _action : myRel) {
                if (action.equals(_action) && !AppUtils.isDiffGreaterThan(action.time, _action.time, EventTrackingServer.getInstance().windowInterval * 2)) {
                    otherAction = action;
                }
            }
        }
        /*if (otherAction != null) {
            otherAction.nbTweets = action.nbTweets;
        }*/
        return otherAction != null;
    }

    void processAction(Event action, Object arg) {
        
        boolean isConfirmed = true;
        ActionStatus status = action.sameAsOneOf(confirmedActions);

        String content = null;
        System.out.println(status + "=>" + action);
        switch (status) {
            case EXISTS:
                //action.confirmedCount++;
                //content = action.toString() + "\tDUP";
                //action.nbTweets = ac
                break;
            case NOT_EXIST:

                if (EventTrackingServer.getInstance().gamePeriod <= 1 && action.type.equals("CGT")) {
                    return;
                }

                if (action.isParticipantRequired() && out.contains(action.player.participant.value.name)) {
                    return;
                }

                content = action.toString();
                action.confirmedCount = 1;
                addAction(action);

                EventTrackingServer.getInstance().gameStatus = action.gameStatus;

                if (!action.inGame) {
                    out.add(action.player.participant.value.surName());
                }
                break;
            case NOT_YET:
                break;

        }
        if (content != null) {
            EventTrackingServer.getInstance().onResult(action);
            AppUtils.logIt(EventTrackingServer.getInstance().game.home + "_" + EventTrackingServer.getInstance().game.away + ".text", content);
        }
    }
}
